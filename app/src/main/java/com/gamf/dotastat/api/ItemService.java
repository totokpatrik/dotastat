package com.gamf.dotastat.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface ItemService {
    @GET("Item")
    Call<String> ListItems(@Header("Authorization") String stringAuth);
}
