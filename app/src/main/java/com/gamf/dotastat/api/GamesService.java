package com.gamf.dotastat.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface GamesService {
    @GET("IDOTA2Match_570/GetMatchHistory/v1")
    Call<String> ListGames(@Query("key") String key,
                           @Query("min_players") Integer min_players,
                           @Query("matches_requested") Integer matches_requested);
}

