package com.gamf.dotastat.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GamesDetailService {
    @GET("IDOTA2Match_570/GetMatchDetails/v1")
    Call<String> ListDetails(@Query("key") String key,
                           @Query("match_id") Long match_id);
}
