package com.gamf.dotastat.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface HeroService {
    @GET("Hero")
    Call<String> ListHeroes(@Header("Authorization") String stringAuth);
}
