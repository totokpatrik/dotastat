package com.gamf.dotastat.models.games;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MatchHistory {
    @SerializedName("result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
