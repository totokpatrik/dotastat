package com.gamf.dotastat.models.heroes;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hero {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("displayName")
    @Expose
    private String displayName;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("abilities")
    @Expose
    private List<Ability> abilities = null;
    @SerializedName("roles")
    @Expose
    private List<Role> roles = null;
    @SerializedName("talents")
    @Expose
    private List<Talent> talents = null;
    @SerializedName("stat")
    @Expose
    private Stat stat;
    @SerializedName("language")
    @Expose
    private Language language;
    @SerializedName("aliases")
    @Expose
    private List<String> aliases = null;
    private String imageURL;

    public Hero (int id,
                 String name,
                 String displayName,
                 String shortName,
                 List<Ability> abilities,
                 List<Role> roles,
                 List<Talent> talents,
                 Stat stat,
                 Language language,
                 List<String> aliases) {
        this.id = id;
        this.name = name;
        this.displayName = displayName;
        this.shortName = shortName;
        this.abilities = abilities;
        this.roles = roles;
        this.talents = talents;
        this.stat = stat;
        this.language = language;
        this.aliases = aliases;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<Ability> abilities) {
        this.abilities = abilities;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public List<Talent> getTalents() {
        return talents;
    }

    public void setTalents(List<Talent> talents) {
        this.talents = talents;
    }

    public Stat getStat() {
        return stat;
    }

    public void setStat(Stat stat) {
        this.stat = stat;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}