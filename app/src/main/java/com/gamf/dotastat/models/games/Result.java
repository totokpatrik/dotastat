package com.gamf.dotastat.models.games;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("num_results")
    @Expose
    private Integer numResults;
    @SerializedName("total_results")
    @Expose
    private Integer totalResults;
    @SerializedName("results_remaining")
    @Expose
    private Integer resultsRemaining;
    @SerializedName("matches")
    @Expose
    private List<Match> matches = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getNumResults() {
        return numResults;
    }

    public void setNumResults(Integer numResults) {
        this.numResults = numResults;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getResultsRemaining() {
        return resultsRemaining;
    }

    public void setResultsRemaining(Integer resultsRemaining) {
        this.resultsRemaining = resultsRemaining;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }
}
