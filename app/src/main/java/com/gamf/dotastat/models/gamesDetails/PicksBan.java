package com.gamf.dotastat.models.gamesDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PicksBan {

    @SerializedName("is_pick")
    @Expose
    private Boolean isPick;
    @SerializedName("hero_id")
    @Expose
    private Integer heroId;
    @SerializedName("team")
    @Expose
    private Integer team;
    @SerializedName("order")
    @Expose
    private Integer order;

    public Boolean getIsPick() {
        return isPick;
    }

    public void setIsPick(Boolean isPick) {
        this.isPick = isPick;
    }

    public Integer getHeroId() {
        return heroId;
    }

    public void setHeroId(Integer heroId) {
        this.heroId = heroId;
    }

    public Integer getTeam() {
        return team;
    }

    public void setTeam(Integer team) {
        this.team = team;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
