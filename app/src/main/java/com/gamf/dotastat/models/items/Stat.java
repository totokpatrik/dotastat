package com.gamf.dotastat.models.items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Stat {
    @SerializedName("behavior")
    @Expose
    private Double behavior;
    @SerializedName("unitTargetType")
    @Expose
    private Double unitTargetType;
    @SerializedName("unitTargetTeam")
    @Expose
    private Double unitTargetTeam;
    @SerializedName("unitTargetFlags")
    @Expose
    private Double unitTargetFlags;
    @SerializedName("fightLevelRecap")
    @Expose
    private Double fightLevelRecap;
    @SerializedName("castRange")
    @Expose
    private List<Double> castRange = null;
    @SerializedName("castPoint")
    @Expose
    private List<Double> castPoint = null;
    @SerializedName("cooldown")
    @Expose
    private List<Double> cooldown = null;
    @SerializedName("manaCost")
    @Expose
    private List<Double> manaCost = null;
    @SerializedName("sharedCooldown")
    @Expose
    private String sharedCooldown;
    @SerializedName("cost")
    @Expose
    private Integer cost;
    @SerializedName("shopTags")
    @Expose
    private String shopTags;
    @SerializedName("aliases")
    @Expose
    private String aliases;
    @SerializedName("quality")
    @Expose
    private String quality;
    @SerializedName("isSellable")
    @Expose
    private Boolean isSellable;
    @SerializedName("isDroppable")
    @Expose
    private Boolean isDroppable;
    @SerializedName("isPurchaseable")
    @Expose
    private Boolean isPurchaseable;
    @SerializedName("isSecretShop")
    @Expose
    private Boolean isSecretShop;
    @SerializedName("isSideShop")
    @Expose
    private Boolean isSideShop;
    @SerializedName("isStackable")
    @Expose
    private Boolean isStackable;
    @SerializedName("isPermanent")
    @Expose
    private Boolean isPermanent;
    @SerializedName("isHideCharges")
    @Expose
    private Boolean isHideCharges;
    @SerializedName("isRequiresCharges")
    @Expose
    private Boolean isRequiresCharges;
    @SerializedName("isDisplayCharges")
    @Expose
    private Boolean isDisplayCharges;
    @SerializedName("isSupport")
    @Expose
    private Boolean isSupport;
    @SerializedName("isTempestDoubleClonable")
    @Expose
    private Boolean isTempestDoubleClonable;
    @SerializedName("stockMax")
    @Expose
    private Integer stockMax;
    @SerializedName("initialCharges")
    @Expose
    private Integer initialCharges;
    @SerializedName("initialStock")
    @Expose
    private Integer initialStock;
    @SerializedName("stockTime")
    @Expose
    private Integer stockTime;
    @SerializedName("initialStockTime")
    @Expose
    private Integer initialStockTime;
    @SerializedName("isRecipe")
    @Expose
    private Boolean isRecipe = false;
    @SerializedName("needsComponents")
    @Expose
    private Boolean needsComponents;

    public Boolean isRecipe() {
        return isRecipe;
    }

    public void setRecipe(Boolean recipe) {
        isRecipe = recipe;
    }

    public void checkRecipe() {
        if (isRecipe == null) {
            isRecipe = false;
        }
    }
}
