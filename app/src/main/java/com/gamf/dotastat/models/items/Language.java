package com.gamf.dotastat.models.items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Language {

    @SerializedName("displayName")
    @Expose
    private String displayName;
    @SerializedName("lore")
    @Expose
    private ArrayList<String> lore = null;
    @SerializedName("description")
    @Expose
    private ArrayList<String> description = null;
    @SerializedName("notes")
    @Expose
    private ArrayList<String> notes = null;
    @SerializedName("attributes")
    @Expose
    private ArrayList<Object> attributes = null;

    public Language(String displayName, ArrayList<String> lore, ArrayList<String> description, ArrayList<String> notes, ArrayList<Object> attributes) {
        this.displayName = displayName;
        this.lore = lore;
        this.description = description;
        this.notes = notes;
        this.attributes = attributes;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public ArrayList<String> getLore() {
        return lore;
    }

    public void setLore(ArrayList<String> lore) {
        this.lore = lore;
    }

    public ArrayList<String> getDescription() {
        return description;
    }

    public void setDescription(ArrayList<String> description) {
        this.description = description;
    }

    public ArrayList<String> getNotes() {
        return notes;
    }

    public void setNotes(ArrayList<String> notes) {
        this.notes = notes;
    }

    public ArrayList<Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<Object> attributes) {
        this.attributes = attributes;
    }
}
