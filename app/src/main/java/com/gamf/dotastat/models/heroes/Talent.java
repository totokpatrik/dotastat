package com.gamf.dotastat.models.heroes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Talent {

    @SerializedName("slot")
    @Expose
    private Integer slot;
    @SerializedName("gameVersionId")
    @Expose
    private Integer gameVersionId;
    @SerializedName("abilityId")
    @Expose
    private Integer abilityId;

    public Integer getSlot() {
        return slot;
    }

    public void setSlot(Integer slot) {
        this.slot = slot;
    }

    public Integer getGameVersionId() {
        return gameVersionId;
    }

    public void setGameVersionId(Integer gameVersionId) {
        this.gameVersionId = gameVersionId;
    }

    public Integer getAbilityId() {
        return abilityId;
    }

    public void setAbilityId(Integer abilityId) {
        this.abilityId = abilityId;
    }

}