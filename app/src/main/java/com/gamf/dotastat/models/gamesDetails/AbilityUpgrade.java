package com.gamf.dotastat.models.gamesDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AbilityUpgrade {
    @SerializedName("ability")
    @Expose
    private Integer ability;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("level")
    @Expose
    private Integer level;

    public Integer getAbility() {
        return ability;
    }

    public void setAbility(Integer ability) {
        this.ability = ability;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
