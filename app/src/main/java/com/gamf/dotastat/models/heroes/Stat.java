package com.gamf.dotastat.models.heroes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stat {

    @SerializedName("gameVersionId")
    @Expose
    private Integer gameVersionId;
    @SerializedName("enabled")
    @Expose
    private Boolean enabled;
    @SerializedName("heroUnlockOrder")
    @Expose
    private Double heroUnlockOrder;
    @SerializedName("team")
    @Expose
    private Boolean team;
    @SerializedName("cmEnabled")
    @Expose
    private Boolean cmEnabled;
    @SerializedName("newPlayerEnabled")
    @Expose
    private Boolean newPlayerEnabled;
    @SerializedName("attackType")
    @Expose
    private String attackType;
    @SerializedName("startingArmor")
    @Expose
    private Double startingArmor;
    @SerializedName("startingMagicArmor")
    @Expose
    private Double startingMagicArmor;
    @SerializedName("startingDamageMin")
    @Expose
    private Double startingDamageMin;
    @SerializedName("startingDamageMax")
    @Expose
    private Double startingDamageMax;
    @SerializedName("attackRate")
    @Expose
    private Double attackRate;
    @SerializedName("attackAnimationPoint")
    @Expose
    private Double attackAnimationPoint;
    @SerializedName("attackAcquisitionRange")
    @Expose
    private Double attackAcquisitionRange;
    @SerializedName("attackRange")
    @Expose
    private Double attackRange;
    @SerializedName("primaryAttribute")
    @Expose
    private String primaryAttribute;
    @SerializedName("heroPrimaryAttribute")
    @Expose
    private Integer heroPrimaryAttribute;
    @SerializedName("strengthBase")
    @Expose
    private Double strengthBase;
    @SerializedName("strengthGain")
    @Expose
    private Double strengthGain;
    @SerializedName("intelligenceBase")
    @Expose
    private Double intelligenceBase;
    @SerializedName("intelligenceGain")
    @Expose
    private Double intelligenceGain;
    @SerializedName("agilityBase")
    @Expose
    private Double agilityBase;
    @SerializedName("agilityGain")
    @Expose
    private Double agilityGain;
    @SerializedName("hpRegen")
    @Expose
    private Double hpRegen;
    @SerializedName("mpRegen")
    @Expose
    private Double mpRegen;
    @SerializedName("moveSpeed")
    @Expose
    private Double moveSpeed;
    @SerializedName("moveTurnRate")
    @Expose
    private Double moveTurnRate;
    @SerializedName("hpBarOffset")
    @Expose
    private Double hpBarOffset;
    @SerializedName("visionDaytimeRange")
    @Expose
    private Double visionDaytimeRange;
    @SerializedName("visionNighttimeRange")
    @Expose
    private Double visionNighttimeRange;
    @SerializedName("complexity")
    @Expose
    private Integer complexity;

    public Integer getGameVersionId() {
        return gameVersionId;
    }

    public void setGameVersionId(Integer gameVersionId) {
        this.gameVersionId = gameVersionId;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Double getHeroUnlockOrder() {
        return heroUnlockOrder;
    }

    public void setHeroUnlockOrder(Double heroUnlockOrder) {
        this.heroUnlockOrder = heroUnlockOrder;
    }

    public Boolean getTeam() {
        return team;
    }

    public void setTeam(Boolean team) {
        this.team = team;
    }

    public Boolean getCmEnabled() {
        return cmEnabled;
    }

    public void setCmEnabled(Boolean cmEnabled) {
        this.cmEnabled = cmEnabled;
    }

    public Boolean getNewPlayerEnabled() {
        return newPlayerEnabled;
    }

    public void setNewPlayerEnabled(Boolean newPlayerEnabled) {
        this.newPlayerEnabled = newPlayerEnabled;
    }

    public String getAttackType() {
        return attackType;
    }

    public void setAttackType(String attackType) {
        this.attackType = attackType;
    }

    public Double getStartingArmor() {
        return startingArmor;
    }

    public void setStartingArmor(Double startingArmor) {
        this.startingArmor = startingArmor;
    }

    public Double getStartingMagicArmor() {
        return startingMagicArmor;
    }

    public void setStartingMagicArmor(Double startingMagicArmor) {
        this.startingMagicArmor = startingMagicArmor;
    }

    public Double getStartingDamageMin() {
        return startingDamageMin;
    }

    public void setStartingDamageMin(Double startingDamageMin) {
        this.startingDamageMin = startingDamageMin;
    }

    public Double getStartingDamageMax() {
        return startingDamageMax;
    }

    public void setStartingDamageMax(Double startingDamageMax) {
        this.startingDamageMax = startingDamageMax;
    }

    public Double getAttackRate() {
        return attackRate;
    }

    public void setAttackRate(Double attackRate) {
        this.attackRate = attackRate;
    }

    public Double getAttackAnimationPoint() {
        return attackAnimationPoint;
    }

    public void setAttackAnimationPoint(Double attackAnimationPoint) {
        this.attackAnimationPoint = attackAnimationPoint;
    }

    public Double getAttackAcquisitionRange() {
        return attackAcquisitionRange;
    }

    public void setAttackAcquisitionRange(Double attackAcquisitionRange) {
        this.attackAcquisitionRange = attackAcquisitionRange;
    }

    public Double getAttackRange() {
        return attackRange;
    }

    public void setAttackRange(Double attackRange) {
        this.attackRange = attackRange;
    }

    public String getPrimaryAttribute() {
        return primaryAttribute;
    }

    public void setPrimaryAttribute(String primaryAttribute) {
        this.primaryAttribute = primaryAttribute;
    }

    public Integer getHeroPrimaryAttribute() {
        return heroPrimaryAttribute;
    }

    public void setHeroPrimaryAttribute(Integer heroPrimaryAttribute) {
        this.heroPrimaryAttribute = heroPrimaryAttribute;
    }

    public Double getStrengthBase() {
        return strengthBase;
    }

    public void setStrengthBase(Double strengthBase) {
        this.strengthBase = strengthBase;
    }

    public Double getStrengthGain() {
        return strengthGain;
    }

    public void setStrengthGain(Double strengthGain) {
        this.strengthGain = strengthGain;
    }

    public Double getIntelligenceBase() {
        return intelligenceBase;
    }

    public void setIntelligenceBase(Double intelligenceBase) {
        this.intelligenceBase = intelligenceBase;
    }

    public Double getIntelligenceGain() {
        return intelligenceGain;
    }

    public void setIntelligenceGain(Double intelligenceGain) {
        this.intelligenceGain = intelligenceGain;
    }

    public Double getAgilityBase() {
        return agilityBase;
    }

    public void setAgilityBase(Double agilityBase) {
        this.agilityBase = agilityBase;
    }

    public Double getAgilityGain() {
        return agilityGain;
    }

    public void setAgilityGain(Double agilityGain) {
        this.agilityGain = agilityGain;
    }

    public Double getHpRegen() {
        return hpRegen;
    }

    public void setHpRegen(Double hpRegen) {
        this.hpRegen = hpRegen;
    }

    public Double getMpRegen() {
        return mpRegen;
    }

    public void setMpRegen(Double mpRegen) {
        this.mpRegen = mpRegen;
    }

    public Double getMoveSpeed() {
        return moveSpeed;
    }

    public void setMoveSpeed(Double moveSpeed) {
        this.moveSpeed = moveSpeed;
    }

    public Double getMoveTurnRate() {
        return moveTurnRate;
    }

    public void setMoveTurnRate(Double moveTurnRate) {
        this.moveTurnRate = moveTurnRate;
    }

    public Double getHpBarOffset() {
        return hpBarOffset;
    }

    public void setHpBarOffset(Double hpBarOffset) {
        this.hpBarOffset = hpBarOffset;
    }

    public Double getVisionDaytimeRange() {
        return visionDaytimeRange;
    }

    public void setVisionDaytimeRange(Double visionDaytimeRange) {
        this.visionDaytimeRange = visionDaytimeRange;
    }

    public Double getVisionNighttimeRange() {
        return visionNighttimeRange;
    }

    public void setVisionNighttimeRange(Double visionNighttimeRange) {
        this.visionNighttimeRange = visionNighttimeRange;
    }

    public Integer getComplexity() {
        return complexity;
    }

    public void setComplexity(Integer complexity) {
        this.complexity = complexity;
    }

}