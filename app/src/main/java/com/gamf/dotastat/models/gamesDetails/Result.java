package com.gamf.dotastat.models.gamesDetails;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("players")
    @Expose
    private List<Player> players = null;
    @SerializedName("radiant_win")
    @Expose
    private Boolean radiantWin;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("pre_game_duration")
    @Expose
    private Integer preGameDuration;
    @SerializedName("start_time")
    @Expose
    private Integer startTime;
    @SerializedName("match_id")
    @Expose
    private Long matchId;
    @SerializedName("match_seq_num")
    @Expose
    private Long matchSeqNum;
    @SerializedName("tower_status_radiant")
    @Expose
    private Integer towerStatusRadiant;
    @SerializedName("tower_status_dire")
    @Expose
    private Integer towerStatusDire;
    @SerializedName("barracks_status_radiant")
    @Expose
    private Integer barracksStatusRadiant;
    @SerializedName("barracks_status_dire")
    @Expose
    private Integer barracksStatusDire;
    @SerializedName("cluster")
    @Expose
    private Integer cluster;
    @SerializedName("first_blood_time")
    @Expose
    private Integer firstBloodTime;
    @SerializedName("lobby_type")
    @Expose
    private Integer lobbyType;
    @SerializedName("human_players")
    @Expose
    private Integer humanPlayers;
    @SerializedName("leagueid")
    @Expose
    private Integer leagueid;
    @SerializedName("positive_votes")
    @Expose
    private Integer positiveVotes;
    @SerializedName("negative_votes")
    @Expose
    private Integer negativeVotes;
    @SerializedName("game_mode")
    @Expose
    private Integer gameMode;
    @SerializedName("flags")
    @Expose
    private Integer flags;
    @SerializedName("engine")
    @Expose
    private Integer engine;
    @SerializedName("radiant_score")
    @Expose
    private Integer radiantScore;
    @SerializedName("dire_score")
    @Expose
    private Integer direScore;
    @SerializedName("picks_bans")
    @Expose
    private List<PicksBan> picksBans = null;

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Boolean getRadiantWin() {
        return radiantWin;
    }

    public void setRadiantWin(Boolean radiantWin) {
        this.radiantWin = radiantWin;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getPreGameDuration() {
        return preGameDuration;
    }

    public void setPreGameDuration(Integer preGameDuration) {
        this.preGameDuration = preGameDuration;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public Long getMatchSeqNum() {
        return matchSeqNum;
    }

    public void setMatchSeqNum(Long matchSeqNum) {
        this.matchSeqNum = matchSeqNum;
    }

    public Integer getTowerStatusRadiant() {
        return towerStatusRadiant;
    }

    public void setTowerStatusRadiant(Integer towerStatusRadiant) {
        this.towerStatusRadiant = towerStatusRadiant;
    }

    public Integer getTowerStatusDire() {
        return towerStatusDire;
    }

    public void setTowerStatusDire(Integer towerStatusDire) {
        this.towerStatusDire = towerStatusDire;
    }

    public Integer getBarracksStatusRadiant() {
        return barracksStatusRadiant;
    }

    public void setBarracksStatusRadiant(Integer barracksStatusRadiant) {
        this.barracksStatusRadiant = barracksStatusRadiant;
    }

    public Integer getBarracksStatusDire() {
        return barracksStatusDire;
    }

    public void setBarracksStatusDire(Integer barracksStatusDire) {
        this.barracksStatusDire = barracksStatusDire;
    }

    public Integer getCluster() {
        return cluster;
    }

    public void setCluster(Integer cluster) {
        this.cluster = cluster;
    }

    public Integer getFirstBloodTime() {
        return firstBloodTime;
    }

    public void setFirstBloodTime(Integer firstBloodTime) {
        this.firstBloodTime = firstBloodTime;
    }

    public Integer getLobbyType() {
        return lobbyType;
    }

    public void setLobbyType(Integer lobbyType) {
        this.lobbyType = lobbyType;
    }

    public Integer getHumanPlayers() {
        return humanPlayers;
    }

    public void setHumanPlayers(Integer humanPlayers) {
        this.humanPlayers = humanPlayers;
    }

    public Integer getLeagueid() {
        return leagueid;
    }

    public void setLeagueid(Integer leagueid) {
        this.leagueid = leagueid;
    }

    public Integer getPositiveVotes() {
        return positiveVotes;
    }

    public void setPositiveVotes(Integer positiveVotes) {
        this.positiveVotes = positiveVotes;
    }

    public Integer getNegativeVotes() {
        return negativeVotes;
    }

    public void setNegativeVotes(Integer negativeVotes) {
        this.negativeVotes = negativeVotes;
    }

    public Integer getGameMode() {
        return gameMode;
    }

    public void setGameMode(Integer gameMode) {
        this.gameMode = gameMode;
    }

    public Integer getFlags() {
        return flags;
    }

    public void setFlags(Integer flags) {
        this.flags = flags;
    }

    public Integer getEngine() {
        return engine;
    }

    public void setEngine(Integer engine) {
        this.engine = engine;
    }

    public Integer getRadiantScore() {
        return radiantScore;
    }

    public void setRadiantScore(Integer radiantScore) {
        this.radiantScore = radiantScore;
    }

    public Integer getDireScore() {
        return direScore;
    }

    public void setDireScore(Integer direScore) {
        this.direScore = direScore;
    }

    public List<PicksBan> getPicksBans() {
        return picksBans;
    }

    public void setPicksBans(List<PicksBan> picksBans) {
        this.picksBans = picksBans;
    }
}
