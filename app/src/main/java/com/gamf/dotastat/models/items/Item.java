package com.gamf.dotastat.models.items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("displayName")
    @Expose
    private String displayName;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("language")
    @Expose
    private Language language;
    @SerializedName("stat")
    @Expose
    private Stat stat;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("isInNeuralNetwork")
    @Expose
    private Boolean isInNeuralNetwork;
    @SerializedName("isFullItemHeroPurchaseItem")
    @Expose
    private Boolean isFullItemHeroPurchaseItem;
    private String imageURL;

    public Item(int id,
                String name,
                String displayName,
                String shortName,
                Language language,
                Stat stat,
                String image,
                Boolean isInNeuralNetwork,
                Boolean isFullItemHeroPurchaseItem) {
        this.id = id;
        this.name = name;
        this.displayName = displayName;
        this.shortName = shortName;
        this.language = language;
        this.stat = stat;
        this.image = image;
        this.isInNeuralNetwork = isInNeuralNetwork;
        this.isFullItemHeroPurchaseItem = isFullItemHeroPurchaseItem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Stat getStat() {
        return stat;
    }

    public void setStat(Stat stat) {
        this.stat = stat;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean isInNeuralNetwork() {
        return isInNeuralNetwork;
    }

    public void setInNeuralNetwork(Boolean inNeuralNetwork) {
        isInNeuralNetwork = inNeuralNetwork;
    }

    public Boolean isFullItemHeroPurchaseItem() {
        return isFullItemHeroPurchaseItem;
    }

    public void setFullItemHeroPurchaseItem(Boolean fullItemHeroPurchaseItem) {
        isFullItemHeroPurchaseItem = fullItemHeroPurchaseItem;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
