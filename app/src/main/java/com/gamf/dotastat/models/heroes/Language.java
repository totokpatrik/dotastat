package com.gamf.dotastat.models.heroes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Language {

    @SerializedName("heroId")
    @Expose
    private Integer heroId;
    @SerializedName("gameVersionId")
    @Expose
    private Integer gameVersionId;
    @SerializedName("languageId")
    @Expose
    private Integer languageId;
    @SerializedName("displayName")
    @Expose
    private String displayName;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("hype")
    @Expose
    private String hype;

    public Integer getHeroId() {
        return heroId;
    }

    public void setHeroId(Integer heroId) {
        this.heroId = heroId;
    }

    public Integer getGameVersionId() {
        return gameVersionId;
    }

    public void setGameVersionId(Integer gameVersionId) {
        this.gameVersionId = gameVersionId;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getHype() {
        return hype;
    }

    public void setHype(String hype) {
        this.hype = hype;
    }

}