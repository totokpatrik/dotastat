package com.gamf.dotastat.models.games;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Player {
    @SerializedName("account_id")
    @Expose
    private Long accountId;
    @SerializedName("player_slot")
    @Expose
    private Integer playerSlot;
    @SerializedName("hero_id")
    @Expose
    private Integer heroId;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getPlayerSlot() {
        return playerSlot;
    }

    public void setPlayerSlot(Integer playerSlot) {
        this.playerSlot = playerSlot;
    }

    public Integer getHeroId() {
        return heroId;
    }

    public void setHeroId(Integer heroId) {
        this.heroId = heroId;
    }
}
