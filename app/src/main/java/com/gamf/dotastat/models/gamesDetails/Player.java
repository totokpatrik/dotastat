package com.gamf.dotastat.models.gamesDetails;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Player {
    @SerializedName("account_id")
    @Expose
    private Long accountId;
    @SerializedName("player_slot")
    @Expose
    private Integer playerSlot;
    @SerializedName("hero_id")
    @Expose
    private Integer heroId;
    private String heroImageURL = null;
    @SerializedName("item_0")
    @Expose
    private Integer item0;
    @SerializedName("item_1")
    @Expose
    private Integer item1;
    @SerializedName("item_2")
    @Expose
    private Integer item2;
    @SerializedName("item_3")
    @Expose
    private Integer item3;
    @SerializedName("item_4")
    @Expose
    private Integer item4;
    @SerializedName("item_5")
    @Expose
    private Integer item5;
    @SerializedName("backpack_0")
    @Expose
    private Integer backpack0;
    @SerializedName("backpack_1")
    @Expose
    private Integer backpack1;
    @SerializedName("backpack_2")
    @Expose
    private Integer backpack2;
    @SerializedName("item_neutral")
    @Expose
    private Integer itemNeutral;
    @SerializedName("kills")
    @Expose
    private Integer kills;
    @SerializedName("deaths")
    @Expose
    private Integer deaths;
    @SerializedName("assists")
    @Expose
    private Integer assists;
    @SerializedName("leaver_status")
    @Expose
    private Integer leaverStatus;
    @SerializedName("last_hits")
    @Expose
    private Integer lastHits;
    @SerializedName("denies")
    @Expose
    private Integer denies;
    @SerializedName("gold_per_min")
    @Expose
    private Integer goldPerMin;
    @SerializedName("xp_per_min")
    @Expose
    private Integer xpPerMin;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("net_worth")
    @Expose
    private Integer netWorth;
    @SerializedName("aghanims_scepter")
    @Expose
    private Integer aghanimsScepter;
    @SerializedName("aghanims_shard")
    @Expose
    private Integer aghanimsShard;
    @SerializedName("moonshard")
    @Expose
    private Integer moonshard;
    @SerializedName("hero_damage")
    @Expose
    private Integer heroDamage;
    @SerializedName("tower_damage")
    @Expose
    private Integer towerDamage;
    @SerializedName("hero_healing")
    @Expose
    private Integer heroHealing;
    @SerializedName("gold")
    @Expose
    private Integer gold;
    @SerializedName("gold_spent")
    @Expose
    private Integer goldSpent;
    @SerializedName("scaled_hero_damage")
    @Expose
    private Integer scaledHeroDamage;
    @SerializedName("scaled_tower_damage")
    @Expose
    private Integer scaledTowerDamage;
    @SerializedName("scaled_hero_healing")
    @Expose
    private Integer scaledHeroHealing;
    @SerializedName("ability_upgrades")
    @Expose
    private List<AbilityUpgrade> abilityUpgrades = null;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getPlayerSlot() {
        return playerSlot;
    }

    public void setPlayerSlot(Integer playerSlot) {
        this.playerSlot = playerSlot;
    }

    public Integer getHeroId() {
        return heroId;
    }

    public void setHeroId(Integer heroId) {
        this.heroId = heroId;
    }

    public Integer getItem0() {
        return item0;
    }

    public void setItem0(Integer item0) {
        this.item0 = item0;
    }

    public Integer getItem1() {
        return item1;
    }

    public void setItem1(Integer item1) {
        this.item1 = item1;
    }

    public Integer getItem2() {
        return item2;
    }

    public void setItem2(Integer item2) {
        this.item2 = item2;
    }

    public Integer getItem3() {
        return item3;
    }

    public void setItem3(Integer item3) {
        this.item3 = item3;
    }

    public Integer getItem4() {
        return item4;
    }

    public void setItem4(Integer item4) {
        this.item4 = item4;
    }

    public Integer getItem5() {
        return item5;
    }

    public void setItem5(Integer item5) {
        this.item5 = item5;
    }

    public Integer getBackpack0() {
        return backpack0;
    }

    public void setBackpack0(Integer backpack0) {
        this.backpack0 = backpack0;
    }

    public Integer getBackpack1() {
        return backpack1;
    }

    public void setBackpack1(Integer backpack1) {
        this.backpack1 = backpack1;
    }

    public Integer getBackpack2() {
        return backpack2;
    }

    public void setBackpack2(Integer backpack2) {
        this.backpack2 = backpack2;
    }

    public Integer getItemNeutral() {
        return itemNeutral;
    }

    public void setItemNeutral(Integer itemNeutral) {
        this.itemNeutral = itemNeutral;
    }

    public Integer getKills() {
        return kills;
    }

    public void setKills(Integer kills) {
        this.kills = kills;
    }

    public Integer getDeaths() {
        return deaths;
    }

    public void setDeaths(Integer deaths) {
        this.deaths = deaths;
    }

    public Integer getAssists() {
        return assists;
    }

    public void setAssists(Integer assists) {
        this.assists = assists;
    }

    public Integer getLeaverStatus() {
        return leaverStatus;
    }

    public void setLeaverStatus(Integer leaverStatus) {
        this.leaverStatus = leaverStatus;
    }

    public Integer getLastHits() {
        return lastHits;
    }

    public void setLastHits(Integer lastHits) {
        this.lastHits = lastHits;
    }

    public Integer getDenies() {
        return denies;
    }

    public void setDenies(Integer denies) {
        this.denies = denies;
    }

    public Integer getGoldPerMin() {
        return goldPerMin;
    }

    public void setGoldPerMin(Integer goldPerMin) {
        this.goldPerMin = goldPerMin;
    }

    public Integer getXpPerMin() {
        return xpPerMin;
    }

    public void setXpPerMin(Integer xpPerMin) {
        this.xpPerMin = xpPerMin;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(Integer netWorth) {
        this.netWorth = netWorth;
    }

    public Integer getAghanimsScepter() {
        return aghanimsScepter;
    }

    public void setAghanimsScepter(Integer aghanimsScepter) {
        this.aghanimsScepter = aghanimsScepter;
    }

    public Integer getAghanimsShard() {
        return aghanimsShard;
    }

    public void setAghanimsShard(Integer aghanimsShard) {
        this.aghanimsShard = aghanimsShard;
    }

    public Integer getMoonshard() {
        return moonshard;
    }

    public void setMoonshard(Integer moonshard) {
        this.moonshard = moonshard;
    }

    public Integer getHeroDamage() {
        return heroDamage;
    }

    public void setHeroDamage(Integer heroDamage) {
        this.heroDamage = heroDamage;
    }

    public Integer getTowerDamage() {
        return towerDamage;
    }

    public void setTowerDamage(Integer towerDamage) {
        this.towerDamage = towerDamage;
    }

    public Integer getHeroHealing() {
        return heroHealing;
    }

    public void setHeroHealing(Integer heroHealing) {
        this.heroHealing = heroHealing;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public Integer getGoldSpent() {
        return goldSpent;
    }

    public void setGoldSpent(Integer goldSpent) {
        this.goldSpent = goldSpent;
    }

    public Integer getScaledHeroDamage() {
        return scaledHeroDamage;
    }

    public void setScaledHeroDamage(Integer scaledHeroDamage) {
        this.scaledHeroDamage = scaledHeroDamage;
    }

    public Integer getScaledTowerDamage() {
        return scaledTowerDamage;
    }

    public void setScaledTowerDamage(Integer scaledTowerDamage) {
        this.scaledTowerDamage = scaledTowerDamage;
    }

    public Integer getScaledHeroHealing() {
        return scaledHeroHealing;
    }

    public void setScaledHeroHealing(Integer scaledHeroHealing) {
        this.scaledHeroHealing = scaledHeroHealing;
    }

    public List<AbilityUpgrade> getAbilityUpgrades() {
        return abilityUpgrades;
    }

    public void setAbilityUpgrades(List<AbilityUpgrade> abilityUpgrades) {
        this.abilityUpgrades = abilityUpgrades;
    }

    public String getHeroImageURL() {
        return heroImageURL;
    }

    public void setHeroImageURL(String heroImageURL) {
        this.heroImageURL = heroImageURL;
    }
}
