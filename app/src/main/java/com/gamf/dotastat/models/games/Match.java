package com.gamf.dotastat.models.games;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Match {
    @SerializedName("match_id")
    @Expose
    private Long matchId;
    @SerializedName("match_seq_num")
    @Expose
    private Long matchSeqNum;
    @SerializedName("start_time")
    @Expose
    private Integer startTime;
    @SerializedName("lobby_type")
    @Expose
    private Integer lobbyType;
    @SerializedName("radiant_team_id")
    @Expose
    private Integer radiantTeamId;
    @SerializedName("dire_team_id")
    @Expose
    private Integer direTeamId;
    @SerializedName("players")
    @Expose
    private List<Player> players = null;

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public Long getMatchSeqNum() {
        return matchSeqNum;
    }

    public void setMatchSeqNum(Long matchSeqNum) {
        this.matchSeqNum = matchSeqNum;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getLobbyType() {
        return lobbyType;
    }

    public void setLobbyType(Integer lobbyType) {
        this.lobbyType = lobbyType;
    }

    public Integer getRadiantTeamId() {
        return radiantTeamId;
    }

    public void setRadiantTeamId(Integer radiantTeamId) {
        this.radiantTeamId = radiantTeamId;
    }

    public Integer getDireTeamId() {
        return direTeamId;
    }

    public void setDireTeamId(Integer direTeamId) {
        this.direTeamId = direTeamId;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

}
