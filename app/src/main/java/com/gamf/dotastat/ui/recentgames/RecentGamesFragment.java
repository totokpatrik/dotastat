package com.gamf.dotastat.ui.recentgames;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gamf.dotastat.databinding.FragmentRecentgamesBinding;
import com.gamf.dotastat.models.games.Match;
import com.gamf.dotastat.models.games.Result;
import com.gamf.dotastat.models.gamesDetails.MatchDetail;
import com.gamf.dotastat.models.heroes.Hero;

import java.util.List;

public class RecentGamesFragment extends Fragment {
    private FragmentRecentgamesBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        RecentGamesViewModel recentGamesViewModel =
                new ViewModelProvider(this).get(RecentGamesViewModel.class);

        binding = FragmentRecentgamesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ((AppCompatActivity)getActivity()).getSupportActionBar().
                setSubtitle("");

        RecyclerView recyclerView = binding.rvGames;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        GamesAdapter gamesAdapter = new GamesAdapter();
        recyclerView.setAdapter(gamesAdapter);


        recentGamesViewModel.getList().observe(getViewLifecycleOwner(), new Observer<List<MatchDetail>>() {
            @Override
            public void onChanged(List<MatchDetail> matches) {
                gamesAdapter.SetMatch((matches));
            }
        });

        recentGamesViewModel.getHeroList().observe(getViewLifecycleOwner(), new Observer<List<Hero>>() {
            @Override
            public void onChanged(List<Hero> heroes) {
                gamesAdapter.SetHeroes(heroes);
            }
        });


        return root;

    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}