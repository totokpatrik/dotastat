package com.gamf.dotastat.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.gamf.dotastat.MainActivity;
import com.gamf.dotastat.databinding.FragmentHomeBinding;
import com.gamf.dotastat.ui.recentgames.RecentGamesFragment;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private ViewPager2 pvHome;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        HomeViewModel homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        pvHome = binding.pvHome;

        HomeAdapter homeAdapter = new HomeAdapter();
        pvHome.setAdapter(homeAdapter);


        ((MainActivity)getActivity()).loadRecentMatches();


        homeViewModel.getImages().observe(getViewLifecycleOwner(), new Observer<int[]>() {
            @Override
            public void onChanged(int[] ints) {
                homeAdapter.SetItems(ints);
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;

    }


}