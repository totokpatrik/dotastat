package com.gamf.dotastat.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gamf.dotastat.R;

public class HomeViewModel extends ViewModel {

    private final MutableLiveData<String> mText;
    private final MutableLiveData<int []> mImageList;

    public HomeViewModel() {
        mText = new MutableLiveData<>();
        mImageList = new MutableLiveData<>();

        mText.setValue("This is home fragment");
        int [] mList = {R.drawable.zeus_heroes, R.drawable.items};
        mImageList.setValue(mList);

    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<int []> getImages() {return mImageList;}
}