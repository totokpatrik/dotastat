package com.gamf.dotastat.ui.heroes;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.gamf.dotastat.models.heroes.Hero;
import com.gamf.dotastat.repositories.HeroRepository;

import java.util.List;

public class HeroesViewModel extends AndroidViewModel {
    private HeroRepository heroRepository;
    private LiveData<List<Hero>> mList;

    public HeroesViewModel(@NonNull Application application) {
        super(application);
        heroRepository = new HeroRepository();
        mList = heroRepository.getItems();
    }

    public LiveData<List<Hero>> getList() { return mList;}
}
