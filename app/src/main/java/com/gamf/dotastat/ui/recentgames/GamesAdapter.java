package com.gamf.dotastat.ui.recentgames;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.gamf.dotastat.MainActivity;
import com.gamf.dotastat.R;
import com.gamf.dotastat.models.games.Match;
import com.gamf.dotastat.models.games.MatchHistory;
import com.gamf.dotastat.models.gamesDetails.MatchDetail;
import com.gamf.dotastat.models.heroes.Hero;
import com.gamf.dotastat.repositories.HeroRepository;

import java.util.ArrayList;
import java.util.List;

public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.GamesHolder> {
    public List<MatchDetail> matches = new ArrayList<>();
    private List<Hero> heroes = new ArrayList<>();

    @NonNull
    @Override
    public GamesAdapter.GamesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_games, parent, false);

        return new GamesHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull GamesAdapter.GamesHolder holder, int position) {
        MatchDetail currentMatch = matches.get(position);

        holder.tvMatchID.setText(currentMatch.getResult().getMatchId().toString());

        int duration = currentMatch.getResult().getDuration();
        int hours = duration / 3600;
        int minutes = (duration % 3600) / 60;
        int seconds = duration % 60;
        String formattedSeconds;
        if (seconds<10) {
            formattedSeconds = "0" + String.valueOf(seconds);
        }
        else {
            formattedSeconds = String.valueOf(seconds);
        }

        if (hours != 0) {
            holder.tvMatchDuration.setText(
                    hours + ":" + minutes + ":" + formattedSeconds
            );
        } else {
            holder.tvMatchDuration.setText(
                    minutes + ":" + formattedSeconds
            );
        }

        if (currentMatch.getResult().getRadiantWin()) {
            holder.tvRadiantVictory.setText("Radiant Victory");
            holder.tvRadiantVictory.setTextColor(Color.parseColor("#92A525"));
        } else {
            holder.tvRadiantVictory.setText("Dire Victory");
            holder.tvRadiantVictory.setTextColor(Color.parseColor("#C23C2A"));
        }


        holder.tvRadiantScore.setText(currentMatch.getResult().getRadiantScore().toString());
        holder.tvDireScore.setText(currentMatch.getResult().getDireScore().toString());

    }

    @Override
    public int getItemCount() {
        return matches.size();
    }

    public void SetMatch(List<MatchDetail> matches) {
        this.matches = matches;
        notifyDataSetChanged();
    }

    public void SetHeroes(List<Hero> heroes) {
        this.heroes = heroes;
        notifyDataSetChanged();
    }

    private MatchDetail GetMatch(Integer position) {
        MatchDetail match = matches.get(position);

        return match;
    }

    public static class GamesHolder extends RecyclerView.ViewHolder {
        public TextView tvMatchID;
        public TextView tvMatchDuration;
        public TextView tvRadiantVictory;
        public TextView tvRadiantScore;
        public TextView tvDireScore;


        public GamesHolder(@NonNull View matchView) {
            super(matchView);

            tvMatchID = matchView.findViewById(R.id.tvMatchID);
            tvMatchDuration = matchView.findViewById(R.id.tvMatchDuration);
            tvRadiantVictory = matchView.findViewById(R.id.tvRadiantVictory);
            tvRadiantScore = matchView.findViewById(R.id.tvRadiantScore);
            tvDireScore = matchView.findViewById(R.id.tvDireScore);



            matchView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    int position = getAbsoluteAdapterPosition();
                    bundle.putString("matchId", String.valueOf(tvMatchID.getText()));
                    Navigation.findNavController(view).navigate(R.id.moveToSpecificFragment, bundle);
                }
            });
        }
    }
}

