package com.gamf.dotastat.ui.recentgames;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gamf.dotastat.R;
import com.gamf.dotastat.databinding.FragmentItemsDetailBinding;
import com.gamf.dotastat.databinding.FragmentRecentGamesDetailBinding;
import com.gamf.dotastat.models.gamesDetails.MatchDetail;
import com.gamf.dotastat.models.gamesDetails.Player;
import com.gamf.dotastat.models.heroes.Hero;
import com.gamf.dotastat.models.items.Item;
import com.gamf.dotastat.repositories.HeroRepository;
import com.gamf.dotastat.ui.heroes.HeroesViewModel;
import com.gamf.dotastat.ui.items.ItemsViewModel;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class RecentGamesDetail extends Fragment {
    private FragmentRecentGamesDetailBinding binding;
    private String position;

    public RecentGamesDetail() {
        // Required empty public constructor
    }

    public static RecentGamesDetail newInstance(String param1, String param2) {
        RecentGamesDetail fragment = new RecentGamesDetail();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecentGamesViewModel recentGamesViewModel =
                new ViewModelProvider(this).get(RecentGamesViewModel.class);
        binding = FragmentRecentGamesDetailBinding.inflate(inflater, container, false);

        HeroesViewModel heroesViewModel =
                new ViewModelProvider(this).get(HeroesViewModel.class);
        ItemsViewModel itemsViewModel =
                new ViewModelProvider(this).get(ItemsViewModel.class);


        View root = binding.getRoot();

        position = getArguments().getString("matchId");

        TextView tvVictory = binding.tvVictory;
        TextView tvScoreRadiant = binding.tvScoreRadiant;
        TextView tvDuration = binding.tvDuration;
        TextView tvScoreDire = binding.tvScoreDire;

        ImageView ivRadiant1 = binding.ivRadiant1;
        ivRadiant1.setTag(R.id.HERO_POSITION, "0");
        ImageView ivRadiant2 = binding.ivRadiant2;
        ivRadiant2.setTag(R.id.HERO_POSITION, "1");
        ImageView ivRadiant3 = binding.ivRadiant3;
        ivRadiant3.setTag(R.id.HERO_POSITION, "2");
        ImageView ivRadiant4 = binding.ivRadiant4;
        ivRadiant4.setTag(R.id.HERO_POSITION, "3");
        ImageView ivRadiant5 = binding.ivRadiant5;
        ivRadiant5.setTag(R.id.HERO_POSITION, "4");
        ImageView ivDire1 = binding.ivDire1;
        ivDire1.setTag(R.id.HERO_POSITION, "128");
        ImageView ivDire2 = binding.ivDire2;
        ivDire2.setTag(R.id.HERO_POSITION, "129");
        ImageView ivDire3 = binding.ivDire3;
        ivDire3.setTag(R.id.HERO_POSITION, "130");
        ImageView ivDire4 = binding.ivDire4;
        ivDire4.setTag(R.id.HERO_POSITION, "131");
        ImageView ivDire5 = binding.ivDire5;
        ivDire5.setTag(R.id.HERO_POSITION, "132");


        List<ImageView> imageList = new ArrayList<>();
        imageList.add(ivRadiant1);
        imageList.add(ivRadiant2);
        imageList.add(ivRadiant3);
        imageList.add(ivRadiant4);
        imageList.add(ivRadiant5);
        imageList.add(ivDire1);
        imageList.add(ivDire2);
        imageList.add(ivDire3);
        imageList.add(ivDire4);
        imageList.add(ivDire5);

        TextView tvRadiant1Player = binding.tvRadiant1Player;
        tvRadiant1Player.setTag(R.id.HERO_POSITION, "0");
        TextView tvRadiant2Player = binding.tvRadiant2Player;
        tvRadiant2Player.setTag(R.id.HERO_POSITION, "1");
        TextView tvRadiant3Player = binding.tvRadiant3Player;
        tvRadiant3Player.setTag(R.id.HERO_POSITION, "2");
        TextView tvRadiant4Player = binding.tvRadiant4Player;
        tvRadiant4Player.setTag(R.id.HERO_POSITION, "3");
        TextView tvRadiant5Player = binding.tvRadiant5Player;
        tvRadiant5Player.setTag(R.id.HERO_POSITION, "4");
        TextView tvDire1Player = binding.tvDire1Player;
        tvDire1Player.setTag(R.id.HERO_POSITION, "128");
        TextView tvDire2Player = binding.tvDire2Player;
        tvDire2Player.setTag(R.id.HERO_POSITION, "129");
        TextView tvDire3Player = binding.tvDire3Player;
        tvDire3Player.setTag(R.id.HERO_POSITION, "130");
        TextView tvDire4Player = binding.tvDire4Player;
        tvDire4Player.setTag(R.id.HERO_POSITION, "131");
        TextView tvDire5Player = binding.tvDire5Player;
        tvDire5Player.setTag(R.id.HERO_POSITION, "132");

        List<TextView> textList = new ArrayList<>();
        textList.add(tvRadiant1Player);
        textList.add(tvRadiant2Player);
        textList.add(tvRadiant3Player);
        textList.add(tvRadiant4Player);
        textList.add(tvRadiant5Player);
        textList.add(tvDire1Player);
        textList.add(tvDire2Player);
        textList.add(tvDire3Player);
        textList.add(tvDire4Player);
        textList.add(tvDire5Player);

        TextView tvRadiant1K = binding.tvRadiant1K;
        tvRadiant1K.setTag(R.id.HERO_POSITION, "0");
        TextView tvRadiant2K = binding.tvRadiant2K;
        tvRadiant2K.setTag(R.id.HERO_POSITION, "1");
        TextView tvRadiant3K = binding.tvRadiant3K;
        tvRadiant3K.setTag(R.id.HERO_POSITION, "2");
        TextView tvRadiant4K = binding.tvRadiant4K;
        tvRadiant4K.setTag(R.id.HERO_POSITION, "3");
        TextView tvRadiant5K = binding.tvRadiant5K;
        tvRadiant5K.setTag(R.id.HERO_POSITION, "4");
        TextView tvDire1K = binding.tvDire1K;
        tvDire1K.setTag(R.id.HERO_POSITION, "128");
        TextView tvDire2K = binding.tvDire2K;
        tvDire2K.setTag(R.id.HERO_POSITION, "129");
        TextView tvDire3K = binding.tvDire3K;
        tvDire3K.setTag(R.id.HERO_POSITION, "130");
        TextView tvDire4K = binding.tvDire4K;
        tvDire4K.setTag(R.id.HERO_POSITION, "131");
        TextView tvDire5K = binding.tvDire5K;
        tvDire5K.setTag(R.id.HERO_POSITION, "132");


        List<TextView> textKills = new ArrayList<>();
        textKills.add(tvRadiant1K);
        textKills.add(tvRadiant2K);
        textKills.add(tvRadiant3K);
        textKills.add(tvRadiant4K);
        textKills.add(tvRadiant5K);
        textKills.add(tvDire1K);
        textKills.add(tvDire2K);
        textKills.add(tvDire3K);
        textKills.add(tvDire4K);
        textKills.add(tvDire5K);

        TextView tvRadiant1D = binding.tvRadiant1D;
        tvRadiant1D.setTag(R.id.HERO_POSITION, "0");
        TextView tvRadiant2D = binding.tvRadiant2D;
        tvRadiant2D.setTag(R.id.HERO_POSITION, "1");
        TextView tvRadiant3D = binding.tvRadiant3D;
        tvRadiant3D.setTag(R.id.HERO_POSITION, "2");
        TextView tvRadiant4D = binding.tvRadiant4D;
        tvRadiant4D.setTag(R.id.HERO_POSITION, "3");
        TextView tvRadiant5D = binding.tvRadiant5D;
        tvRadiant5D.setTag(R.id.HERO_POSITION, "4");
        TextView tvDire1D = binding.tvDire1D;
        tvDire1D.setTag(R.id.HERO_POSITION, "128");
        TextView tvDire2D = binding.tvDire2D;
        tvDire2D.setTag(R.id.HERO_POSITION, "129");
        TextView tvDire3D = binding.tvDire3D;
        tvDire3D.setTag(R.id.HERO_POSITION, "130");
        TextView tvDire4D = binding.tvDire4D;
        tvDire4D.setTag(R.id.HERO_POSITION, "131");
        TextView tvDire5D = binding.tvDire5D;
        tvDire5D.setTag(R.id.HERO_POSITION, "132");


        List<TextView> textDeaths = new ArrayList<>();
        textDeaths.add(tvRadiant1D);
        textDeaths.add(tvRadiant2D);
        textDeaths.add(tvRadiant3D);
        textDeaths.add(tvRadiant4D);
        textDeaths.add(tvRadiant5D);
        textDeaths.add(tvDire1D);
        textDeaths.add(tvDire2D);
        textDeaths.add(tvDire3D);
        textDeaths.add(tvDire4D);
        textDeaths.add(tvDire5D);

        TextView tvRadiant1A = binding.tvRadiant1A;
        tvRadiant1A.setTag(R.id.HERO_POSITION, "0");
        TextView tvRadiant2A = binding.tvRadiant2A;
        tvRadiant2A.setTag(R.id.HERO_POSITION, "1");
        TextView tvRadiant3A = binding.tvRadiant3A;
        tvRadiant3A.setTag(R.id.HERO_POSITION, "2");
        TextView tvRadiant4A = binding.tvRadiant4A;
        tvRadiant4A.setTag(R.id.HERO_POSITION, "3");
        TextView tvRadiant5A = binding.tvRadiant5A;
        tvRadiant5A.setTag(R.id.HERO_POSITION, "4");
        TextView tvDire1A = binding.tvDire1A;
        tvDire1A.setTag(R.id.HERO_POSITION, "128");
        TextView tvDire2A = binding.tvDire2A;
        tvDire2A.setTag(R.id.HERO_POSITION, "129");
        TextView tvDire3A = binding.tvDire3A;
        tvDire3A.setTag(R.id.HERO_POSITION, "130");
        TextView tvDire4A = binding.tvDire4A;
        tvDire4A.setTag(R.id.HERO_POSITION, "131");
        TextView tvDire5A = binding.tvDire5A;
        tvDire5A.setTag(R.id.HERO_POSITION, "132");


        List<TextView> textAssists = new ArrayList<>();
        textAssists.add(tvRadiant1A);
        textAssists.add(tvRadiant2A);
        textAssists.add(tvRadiant3A);
        textAssists.add(tvRadiant4A);
        textAssists.add(tvRadiant5A);
        textAssists.add(tvDire1A);
        textAssists.add(tvDire2A);
        textAssists.add(tvDire3A);
        textAssists.add(tvDire4A);
        textAssists.add(tvDire5A);

        TextView tvRadiant1NET = binding.tvRadiant1NET;
        tvRadiant1NET.setTag(R.id.HERO_POSITION, "0");
        TextView tvRadiant2NET = binding.tvRadiant2NET;
        tvRadiant2NET.setTag(R.id.HERO_POSITION, "1");
        TextView tvRadiant3NET = binding.tvRadiant3NET;
        tvRadiant3NET.setTag(R.id.HERO_POSITION, "2");
        TextView tvRadiant4NET = binding.tvRadiant4NET;
        tvRadiant4NET.setTag(R.id.HERO_POSITION, "3");
        TextView tvRadiant5NET = binding.tvRadiant5NET;
        tvRadiant5NET.setTag(R.id.HERO_POSITION, "4");
        TextView tvDire1NET = binding.tvDire1NET;
        tvDire1NET.setTag(R.id.HERO_POSITION, "128");
        TextView tvDire2NET = binding.tvDire2NET;
        tvDire2NET.setTag(R.id.HERO_POSITION, "129");
        TextView tvDire3NET = binding.tvDire3NET;
        tvDire3NET.setTag(R.id.HERO_POSITION, "130");
        TextView tvDire4NET = binding.tvDire4NET;
        tvDire4NET.setTag(R.id.HERO_POSITION, "131");
        TextView tvDire5NET = binding.tvDire5NET;
        tvDire5NET.setTag(R.id.HERO_POSITION, "132");


        List<TextView> textNET = new ArrayList<>();
        textNET.add(tvRadiant1NET);
        textNET.add(tvRadiant2NET);
        textNET.add(tvRadiant3NET);
        textNET.add(tvRadiant4NET);
        textNET.add(tvRadiant5NET);
        textNET.add(tvDire1NET);
        textNET.add(tvDire2NET);
        textNET.add(tvDire3NET);
        textNET.add(tvDire4NET);
        textNET.add(tvDire5NET);

        TextView tvRadiant1LHD = binding.tvRadiant1LHD;
        tvRadiant1LHD.setTag(R.id.HERO_POSITION, "0");
        TextView tvRadiant2LHD = binding.tvRadiant2LHD;
        tvRadiant2LHD.setTag(R.id.HERO_POSITION, "1");
        TextView tvRadiant3LHD = binding.tvRadiant3LHD;
        tvRadiant3LHD.setTag(R.id.HERO_POSITION, "2");
        TextView tvRadiant4LHD = binding.tvRadiant4LHD;
        tvRadiant4LHD.setTag(R.id.HERO_POSITION, "3");
        TextView tvRadiant5LHD = binding.tvRadiant5LHD;
        tvRadiant5LHD.setTag(R.id.HERO_POSITION, "4");
        TextView tvDire1LHD = binding.tvDire1LHD;
        tvDire1LHD.setTag(R.id.HERO_POSITION, "128");
        TextView tvDire2LHD = binding.tvDire2LHD;
        tvDire2LHD.setTag(R.id.HERO_POSITION, "129");
        TextView tvDire3LHD = binding.tvDire3LHD;
        tvDire3LHD.setTag(R.id.HERO_POSITION, "130");
        TextView tvDire4LHD = binding.tvDire4LHD;
        tvDire4LHD.setTag(R.id.HERO_POSITION, "131");
        TextView tvDire5LHD = binding.tvDire5LHD;
        tvDire5LHD.setTag(R.id.HERO_POSITION, "132");


        List<TextView> textLHD = new ArrayList<>();
        textLHD.add(tvRadiant1LHD);
        textLHD.add(tvRadiant2LHD);
        textLHD.add(tvRadiant3LHD);
        textLHD.add(tvRadiant4LHD);
        textLHD.add(tvRadiant5LHD);
        textLHD.add(tvDire1LHD);
        textLHD.add(tvDire2LHD);
        textLHD.add(tvDire3LHD);
        textLHD.add(tvDire4LHD);
        textLHD.add(tvDire5LHD);

        ImageView ivRadiant1Item1 = binding.ivRadiant1Item1;
        ivRadiant1Item1.setTag(R.id.HERO_POSITION, "0");
        ImageView ivRadiant2Item1 = binding.ivRadiant2Item1;
        ivRadiant2Item1.setTag(R.id.HERO_POSITION, "1");
        ImageView ivRadiant3Item1 = binding.ivRadiant3Item1;
        ivRadiant3Item1.setTag(R.id.HERO_POSITION, "2");
        ImageView ivRadiant4Item1 = binding.ivRadiant4Item1;
        ivRadiant4Item1.setTag(R.id.HERO_POSITION, "3");
        ImageView ivRadiant5Item1 = binding.ivRadiant5Item1;
        ivRadiant5Item1.setTag(R.id.HERO_POSITION, "4");
        ImageView ivDire1Item1 = binding.ivDire1Item1;
        ivDire1Item1.setTag(R.id.HERO_POSITION, "128");
        ImageView ivDire2Item1 = binding.ivDire2Item1;
        ivDire2Item1.setTag(R.id.HERO_POSITION, "129");
        ImageView ivDire3Item1 = binding.ivDire3Item1;
        ivDire3Item1.setTag(R.id.HERO_POSITION, "130");
        ImageView ivDire4Item1 = binding.ivDire4Item1;
        ivDire4Item1.setTag(R.id.HERO_POSITION, "131");
        ImageView ivDire5Item1 = binding.ivDire5Item1;
        ivDire5Item1.setTag(R.id.HERO_POSITION, "132");


        List<ImageView> imageListItem1 = new ArrayList<>();
        imageListItem1.add(ivRadiant1Item1);
        imageListItem1.add(ivRadiant2Item1);
        imageListItem1.add(ivRadiant3Item1);
        imageListItem1.add(ivRadiant4Item1);
        imageListItem1.add(ivRadiant5Item1);
        imageListItem1.add(ivDire1Item1);
        imageListItem1.add(ivDire2Item1);
        imageListItem1.add(ivDire3Item1);
        imageListItem1.add(ivDire4Item1);
        imageListItem1.add(ivDire5Item1);


        ImageView ivRadiant1Item2 = binding.ivRadiant1Item2;
        ivRadiant1Item2.setTag(R.id.HERO_POSITION, "0");
        ImageView ivRadiant2Item2 = binding.ivRadiant2Item2;
        ivRadiant2Item2.setTag(R.id.HERO_POSITION, "1");
        ImageView ivRadiant3Item2 = binding.ivRadiant3Item2;
        ivRadiant3Item2.setTag(R.id.HERO_POSITION, "2");
        ImageView ivRadiant4Item2 = binding.ivRadiant4Item2;
        ivRadiant4Item2.setTag(R.id.HERO_POSITION, "3");
        ImageView ivRadiant5Item2 = binding.ivRadiant5Item2;
        ivRadiant5Item2.setTag(R.id.HERO_POSITION, "4");
        ImageView ivDire1Item2 = binding.ivDire1Item2;
        ivDire1Item2.setTag(R.id.HERO_POSITION, "128");
        ImageView ivDire2Item2 = binding.ivDire2Item2;
        ivDire2Item2.setTag(R.id.HERO_POSITION, "129");
        ImageView ivDire3Item2 = binding.ivDire3Item2;
        ivDire3Item2.setTag(R.id.HERO_POSITION, "130");
        ImageView ivDire4Item2 = binding.ivDire4Item2;
        ivDire4Item2.setTag(R.id.HERO_POSITION, "131");
        ImageView ivDire5Item2 = binding.ivDire5Item2;
        ivDire5Item2.setTag(R.id.HERO_POSITION, "132");


        List<ImageView> imageListItem2 = new ArrayList<>();
        imageListItem2.add(ivRadiant1Item2);
        imageListItem2.add(ivRadiant2Item2);
        imageListItem2.add(ivRadiant3Item2);
        imageListItem2.add(ivRadiant4Item2);
        imageListItem2.add(ivRadiant5Item2);
        imageListItem2.add(ivDire1Item2);
        imageListItem2.add(ivDire2Item2);
        imageListItem2.add(ivDire3Item2);
        imageListItem2.add(ivDire4Item2);
        imageListItem2.add(ivDire5Item2);

        ImageView ivRadiant1Item3 = binding.ivRadiant1Item3;
        ivRadiant1Item3.setTag(R.id.HERO_POSITION, "0");
        ImageView ivRadiant2Item3 = binding.ivRadiant2Item3;
        ivRadiant2Item3.setTag(R.id.HERO_POSITION, "1");
        ImageView ivRadiant3Item3 = binding.ivRadiant3Item3;
        ivRadiant3Item3.setTag(R.id.HERO_POSITION, "2");
        ImageView ivRadiant4Item3 = binding.ivRadiant4Item3;
        ivRadiant4Item3.setTag(R.id.HERO_POSITION, "3");
        ImageView ivRadiant5Item3 = binding.ivRadiant5Item3;
        ivRadiant5Item3.setTag(R.id.HERO_POSITION, "4");
        ImageView ivDire1Item3 = binding.ivDire1Item3;
        ivDire1Item3.setTag(R.id.HERO_POSITION, "128");
        ImageView ivDire2Item3 = binding.ivDire2Item3;
        ivDire2Item3.setTag(R.id.HERO_POSITION, "129");
        ImageView ivDire3Item3 = binding.ivDire3Item3;
        ivDire3Item3.setTag(R.id.HERO_POSITION, "130");
        ImageView ivDire4Item3 = binding.ivDire4Item3;
        ivDire4Item3.setTag(R.id.HERO_POSITION, "131");
        ImageView ivDire5Item3 = binding.ivDire5Item3;
        ivDire5Item3.setTag(R.id.HERO_POSITION, "132");


        List<ImageView> imageListItem3 = new ArrayList<>();
        imageListItem3.add(ivRadiant1Item3);
        imageListItem3.add(ivRadiant2Item3);
        imageListItem3.add(ivRadiant3Item3);
        imageListItem3.add(ivRadiant4Item3);
        imageListItem3.add(ivRadiant5Item3);
        imageListItem3.add(ivDire1Item3);
        imageListItem3.add(ivDire2Item3);
        imageListItem3.add(ivDire3Item3);
        imageListItem3.add(ivDire4Item3);
        imageListItem3.add(ivDire5Item3);

        ImageView ivRadiant1Item4 = binding.ivRadiant1Item4;
        ivRadiant1Item4.setTag(R.id.HERO_POSITION, "0");
        ImageView ivRadiant2Item4 = binding.ivRadiant2Item4;
        ivRadiant2Item4.setTag(R.id.HERO_POSITION, "1");
        ImageView ivRadiant3Item4 = binding.ivRadiant3Item4;
        ivRadiant3Item4.setTag(R.id.HERO_POSITION, "2");
        ImageView ivRadiant4Item4 = binding.ivRadiant4Item4;
        ivRadiant4Item4.setTag(R.id.HERO_POSITION, "3");
        ImageView ivRadiant5Item4 = binding.ivRadiant5Item4;
        ivRadiant5Item4.setTag(R.id.HERO_POSITION, "4");
        ImageView ivDire1Item4 = binding.ivDire1Item4;
        ivDire1Item4.setTag(R.id.HERO_POSITION, "128");
        ImageView ivDire2Item4 = binding.ivDire2Item4;
        ivDire2Item4.setTag(R.id.HERO_POSITION, "129");
        ImageView ivDire3Item4 = binding.ivDire3Item4;
        ivDire3Item4.setTag(R.id.HERO_POSITION, "130");
        ImageView ivDire4Item4 = binding.ivDire4Item4;
        ivDire4Item4.setTag(R.id.HERO_POSITION, "131");
        ImageView ivDire5Item4 = binding.ivDire5Item4;
        ivDire5Item4.setTag(R.id.HERO_POSITION, "132");


        List<ImageView> imageListItem4 = new ArrayList<>();
        imageListItem4.add(ivRadiant1Item4);
        imageListItem4.add(ivRadiant2Item4);
        imageListItem4.add(ivRadiant3Item4);
        imageListItem4.add(ivRadiant4Item4);
        imageListItem4.add(ivRadiant5Item4);
        imageListItem4.add(ivDire1Item4);
        imageListItem4.add(ivDire2Item4);
        imageListItem4.add(ivDire3Item4);
        imageListItem4.add(ivDire4Item4);
        imageListItem4.add(ivDire5Item4);

        ImageView ivRadiant1Item5 = binding.ivRadiant1Item5;
        ivRadiant1Item5.setTag(R.id.HERO_POSITION, "0");
        ImageView ivRadiant2Item5 = binding.ivRadiant2Item5;
        ivRadiant2Item5.setTag(R.id.HERO_POSITION, "1");
        ImageView ivRadiant3Item5 = binding.ivRadiant3Item5;
        ivRadiant3Item5.setTag(R.id.HERO_POSITION, "2");
        ImageView ivRadiant4Item5 = binding.ivRadiant4Item5;
        ivRadiant4Item5.setTag(R.id.HERO_POSITION, "3");
        ImageView ivRadiant5Item5 = binding.ivRadiant5Item5;
        ivRadiant5Item5.setTag(R.id.HERO_POSITION, "4");
        ImageView ivDire1Item5 = binding.ivDire1Item5;
        ivDire1Item5.setTag(R.id.HERO_POSITION, "128");
        ImageView ivDire2Item5 = binding.ivDire2Item5;
        ivDire2Item5.setTag(R.id.HERO_POSITION, "129");
        ImageView ivDire3Item5 = binding.ivDire3Item5;
        ivDire3Item5.setTag(R.id.HERO_POSITION, "130");
        ImageView ivDire4Item5 = binding.ivDire4Item5;
        ivDire4Item5.setTag(R.id.HERO_POSITION, "131");
        ImageView ivDire5Item5 = binding.ivDire5Item5;
        ivDire5Item5.setTag(R.id.HERO_POSITION, "132");


        List<ImageView> imageListItem5 = new ArrayList<>();
        imageListItem5.add(ivRadiant1Item5);
        imageListItem5.add(ivRadiant2Item5);
        imageListItem5.add(ivRadiant3Item5);
        imageListItem5.add(ivRadiant4Item5);
        imageListItem5.add(ivRadiant5Item5);
        imageListItem5.add(ivDire1Item5);
        imageListItem5.add(ivDire2Item5);
        imageListItem5.add(ivDire3Item5);
        imageListItem5.add(ivDire4Item5);
        imageListItem5.add(ivDire5Item5);

        ImageView ivRadiant1Item6 = binding.ivRadiant1Item6;
        ivRadiant1Item6.setTag(R.id.HERO_POSITION, "0");
        ImageView ivRadiant2Item6 = binding.ivRadiant2Item6;
        ivRadiant2Item6.setTag(R.id.HERO_POSITION, "1");
        ImageView ivRadiant3Item6 = binding.ivRadiant3Item6;
        ivRadiant3Item6.setTag(R.id.HERO_POSITION, "2");
        ImageView ivRadiant4Item6 = binding.ivRadiant4Item6;
        ivRadiant4Item6.setTag(R.id.HERO_POSITION, "3");
        ImageView ivRadiant5Item6 = binding.ivRadiant5Item6;
        ivRadiant5Item6.setTag(R.id.HERO_POSITION, "4");
        ImageView ivDire1Item6 = binding.ivDire1Item6;
        ivDire1Item6.setTag(R.id.HERO_POSITION, "128");
        ImageView ivDire2Item6 = binding.ivDire2Item6;
        ivDire2Item6.setTag(R.id.HERO_POSITION, "129");
        ImageView ivDire3Item6 = binding.ivDire3Item6;
        ivDire3Item6.setTag(R.id.HERO_POSITION, "130");
        ImageView ivDire4Item6 = binding.ivDire4Item6;
        ivDire4Item6.setTag(R.id.HERO_POSITION, "131");
        ImageView ivDire5Item6 = binding.ivDire5Item6;
        ivDire5Item6.setTag(R.id.HERO_POSITION, "132");


        List<ImageView> imageListItem6 = new ArrayList<>();
        imageListItem6.add(ivRadiant1Item6);
        imageListItem6.add(ivRadiant2Item6);
        imageListItem6.add(ivRadiant3Item6);
        imageListItem6.add(ivRadiant4Item6);
        imageListItem6.add(ivRadiant5Item6);
        imageListItem6.add(ivDire1Item6);
        imageListItem6.add(ivDire2Item6);
        imageListItem6.add(ivDire3Item6);
        imageListItem6.add(ivDire4Item6);
        imageListItem6.add(ivDire5Item6);


        heroesViewModel.getList().observe(getViewLifecycleOwner(), new Observer<List<Hero>>() {
            @Override
            public void onChanged(List<Hero> heroes) {
                recentGamesViewModel.getSpecificGame(Long.parseLong(position)).observe(getViewLifecycleOwner(), new Observer<MatchDetail>() {
                    @Override
                    public void onChanged(MatchDetail matchDetail) {
                        itemsViewModel.getList().observe(getViewLifecycleOwner(), new Observer<List<Item>>() {
                            @Override
                            public void onChanged(List<Item> items) {
                                ((AppCompatActivity) getActivity()).getSupportActionBar().
                                        setSubtitle(matchDetail.getResult().getMatchId().toString());

                                if (matchDetail.getResult().getRadiantWin()) {
                                    tvVictory.setText("RADIANT VICTORY");
                                    tvVictory.setTextColor(Color.parseColor("#92A525"));
                                } else {
                                    tvVictory.setText("DIRE VICTORY");
                                    tvVictory.setTextColor(Color.parseColor("#C23C2A"));
                                }

                                tvScoreRadiant.setText(matchDetail.getResult().getRadiantScore().toString());


                                int duration = matchDetail.getResult().getDuration();
                                int hours = duration / 3600;
                                int minutes = (duration % 3600) / 60;
                                int seconds = duration % 60;
                                String formattedSeconds;
                                if (seconds < 10) {
                                    formattedSeconds = "0" + String.valueOf(seconds);
                                } else {
                                    formattedSeconds = String.valueOf(seconds);
                                }

                                if (hours != 0) {
                                    tvDuration.setText(
                                            hours + ":" + minutes + ":" + formattedSeconds
                                    );
                                } else {
                                    tvDuration.setText(
                                            minutes + ":" + formattedSeconds
                                    );
                                }

                                tvScoreDire.setText(matchDetail.getResult().getDireScore().toString());

                                //Hero images

                                for (Player player : matchDetail.getResult().getPlayers()) {
                                    for (ImageView iv : imageList) {
                                        if (Integer.parseInt(iv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            for (Hero hero : heroes) {

                                                if (hero.getId().toString().equals(player.getHeroId().toString())) {
                                                    Glide.with(root)
                                                            .load(hero.getImageURL())
                                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                            .placeholder(R.drawable.ic_profile)
                                                            .error(R.drawable.snapfire_horz)
                                                            .into(iv);
                                                }
                                            }
                                        }
                                    }
                                }

                                //Player informations
                                for (Player player : matchDetail.getResult().getPlayers()) {
                                    for (TextView tv : textList) {
                                        if (Integer.parseInt(tv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            if (player.getAccountId() != Long.parseLong("4294967295")) {
                                                tv.setText(player.getAccountId().toString());
                                            } else {
                                                tv.setText("Anonymus");
                                            }

                                        }
                                    }
                                }

                                //Player kills
                                for (Player player : matchDetail.getResult().getPlayers()) {
                                    for (TextView tv : textKills) {
                                        if (Integer.parseInt(tv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            tv.setText(player.getKills().toString());
                                        }
                                    }
                                }

                                //Player deaths
                                for (Player player : matchDetail.getResult().getPlayers()) {
                                    for (TextView tv : textDeaths) {
                                        if (Integer.parseInt(tv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            tv.setText(player.getDeaths().toString());
                                        }
                                    }
                                }

                                //Player assists
                                for (Player player : matchDetail.getResult().getPlayers()) {
                                    for (TextView tv : textAssists) {
                                        if (Integer.parseInt(tv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            tv.setText(player.getAssists().toString());
                                        }
                                    }
                                }

                                //Player networth
                                for (Player player : matchDetail.getResult().getPlayers()) {
                                    for (TextView tv : textNET) {
                                        if (Integer.parseInt(tv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            Double networth = player.getNetWorth() / 1000.0;
                                            DecimalFormat f = new DecimalFormat("##.##k");
                                            if (player.getNetWorth()> 1000) {
                                                tv.setText(f.format(networth));
                                            } else {
                                                tv.setText(player.getNetWorth().toString());
                                            }

                                        }
                                    }
                                }

                                //Player lasthit/deny
                                for (Player player : matchDetail.getResult().getPlayers()) {
                                    for (TextView tv : textLHD) {
                                        if (Integer.parseInt(tv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            String lasthit = player.getLastHits().toString();
                                            String deny = player.getDenies().toString();
                                            tv.setText(lasthit + " / " + deny);
                                        }
                                    }
                                }


                                //Player item images
                                for (Player player : matchDetail.getResult().getPlayers()) {
                                    for (ImageView iv : imageListItem1) {
                                        if (Integer.parseInt(iv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            if (player.getItem0().toString().equals("0")) {
                                                iv.setVisibility(View.GONE);
                                            }
                                            for (Item item : items) {
                                                if (item.getId().toString().equals(player.getItem0().toString())) {
                                                    Glide.with(root)
                                                            .load(item.getImageURL())
                                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                            .placeholder(R.drawable.ic_profile)
                                                            .error(R.drawable.snapfire_horz)
                                                            .into(iv);
                                                }
                                            }
                                        }
                                    }
                                    for (ImageView iv : imageListItem2) {
                                        if (Integer.parseInt(iv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            if (player.getItem1().toString().equals("0")) {
                                                iv.setVisibility(View.GONE);
                                            }
                                            for (Item item : items) {
                                                if (item.getId().toString().equals(player.getItem1().toString())) {
                                                    Glide.with(root)
                                                            .load(item.getImageURL())
                                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                            .placeholder(R.drawable.ic_profile)
                                                            .error(R.drawable.snapfire_horz)
                                                            .into(iv);
                                                }
                                            }
                                        }
                                    }
                                    for (ImageView iv : imageListItem3) {
                                        if (Integer.parseInt(iv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            if (player.getItem2().toString().equals("0")) {
                                                iv.setVisibility(View.GONE);
                                            }
                                            for (Item item : items) {
                                                if (item.getId().toString().equals(player.getItem2().toString())) {
                                                    Glide.with(root)
                                                            .load(item.getImageURL())
                                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                            .placeholder(R.drawable.ic_profile)
                                                            .error(R.drawable.snapfire_horz)
                                                            .into(iv);
                                                }
                                            }
                                        }
                                    }
                                    for (ImageView iv : imageListItem4) {
                                        if (Integer.parseInt(iv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            if (player.getItem3().toString().equals("0")) {
                                                iv.setVisibility(View.GONE);
                                            }
                                            for (Item item : items) {
                                                if (item.getId().toString().equals(player.getItem3().toString())) {
                                                    Glide.with(root)
                                                            .load(item.getImageURL())
                                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                            .placeholder(R.drawable.ic_profile)
                                                            .error(R.drawable.snapfire_horz)
                                                            .into(iv);
                                                }
                                            }
                                        }
                                    }
                                    for (ImageView iv : imageListItem5) {
                                        if (Integer.parseInt(iv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            if (player.getItem4().toString().equals("0")) {
                                                iv.setVisibility(View.GONE);
                                            }
                                            for (Item item : items) {
                                                if (item.getId().toString().equals(player.getItem4().toString())) {
                                                    Glide.with(root)
                                                            .load(item.getImageURL())
                                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                            .placeholder(R.drawable.ic_profile)
                                                            .error(R.drawable.snapfire_horz)
                                                            .into(iv);
                                                }
                                            }
                                        }
                                    }
                                    for (ImageView iv : imageListItem6) {
                                        if (Integer.parseInt(iv.getTag(R.id.HERO_POSITION).toString()) == player.getPlayerSlot()) {
                                            if (player.getItem5().toString().equals("0")) {
                                                iv.setVisibility(View.GONE);
                                            }
                                            for (Item item : items) {
                                                if (item.getId().toString().equals(player.getItem5().toString())) {
                                                    Glide.with(root)
                                                            .load(item.getImageURL())
                                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                            .placeholder(R.drawable.ic_profile)
                                                            .error(R.drawable.snapfire_horz)
                                                            .into(iv);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });


        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((AppCompatActivity) getActivity()).getSupportActionBar().
                setSubtitle("");
    }
}