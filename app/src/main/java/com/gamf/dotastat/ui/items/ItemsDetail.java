package com.gamf.dotastat.ui.items;

import android.media.Image;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gamf.dotastat.R;
import com.gamf.dotastat.databinding.FragmentItemsBinding;
import com.gamf.dotastat.databinding.FragmentItemsDetailBinding;
import com.gamf.dotastat.models.items.Item;

import org.w3c.dom.Text;

import java.util.List;

public class ItemsDetail extends Fragment {
    private FragmentItemsDetailBinding binding;
    private int position;

    public ItemsDetail() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ItemsViewModel itemsViewModel =
                new ViewModelProvider(this).get(ItemsViewModel.class);

        binding = FragmentItemsDetailBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        position = Integer.parseInt(getArguments().getString("position"));

        String notFound = getResources().getString(R.string.not_found);

        TextView tvDisplayName = binding.tvDisplayName;
        TextView tvLore = binding.tvLore;
        TextView tvDescription = binding.tvDescription;
        TextView tvNotes = binding.tvNotes;

        ImageView ivItem = binding.ivItem;

        itemsViewModel.getList().observe(getViewLifecycleOwner(), new Observer<List<Item>>() {
            @Override
            public void onChanged(List<Item> items) {

                ((AppCompatActivity)getActivity()).getSupportActionBar().
                        setSubtitle(items.get(position).getDisplayName());

                Glide.with(root)
                        .load(items.get(position).getImageURL())
                        .into(ivItem);

                tvDisplayName.setText(items.get(position).getDisplayName());


                if (items.get(position).getLanguage().getLore().size() > 0) {
                    tvLore.setText(
                            Html.fromHtml(
                                    items.get(position).getLanguage().getLore().get(0)));
                }
                else {
                    tvLore.setText(notFound);
                }


                if (items.get(position).getLanguage().getDescription().size() > 0) {
                    tvDescription.setText(
                            Html.fromHtml(
                                    items.get(position).getLanguage().getDescription().get(0)));
                } else {
                    tvDescription.setText(notFound);
                }


                if (items.get(position).getLanguage().getNotes().size() > 0) {
                    tvNotes.setText(
                            Html.fromHtml(
                                    items.get(position).getLanguage().getNotes().get(0)));
                } else {
                    tvNotes.setText(notFound);
                }

            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}