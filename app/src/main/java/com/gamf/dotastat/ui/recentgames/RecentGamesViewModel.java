package com.gamf.dotastat.ui.recentgames;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.gamf.dotastat.models.gamesDetails.MatchDetail;
import com.gamf.dotastat.models.gamesDetails.Player;
import com.gamf.dotastat.models.heroes.Hero;
import com.gamf.dotastat.repositories.GamesRepository;
import com.gamf.dotastat.repositories.HeroRepository;

import java.util.List;

public class RecentGamesViewModel extends AndroidViewModel {
    private GamesRepository gamesRepository;
    private LiveData<List<MatchDetail>> mList;
    private LiveData<MatchDetail> specificGame;

    private HeroRepository heroRepository;
    private LiveData<List<Hero>> mHeroList;

    public RecentGamesViewModel(@NonNull Application application) {
        super(application);
        gamesRepository = new GamesRepository();
        heroRepository = new HeroRepository();


        mList = gamesRepository.getGames();
        mHeroList = heroRepository.getItems();

    }

    public LiveData<List<MatchDetail>> getList() {
        return mList;
    }

    public LiveData<List<Hero>> getHeroList() {
        return mHeroList;
    }

    public LiveData<MatchDetail> getSpecificGame(Long matchId) {
        specificGame = gamesRepository.getSpecificGame(matchId);
        return specificGame;
    }

}