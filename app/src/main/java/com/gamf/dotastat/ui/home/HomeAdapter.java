package com.gamf.dotastat.ui.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.NavHostController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.RecyclerView;

import com.gamf.dotastat.R;
import com.gamf.dotastat.ui.items.ItemsAdapter;
import com.gamf.dotastat.ui.items.ItemsFragment;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
    private int [] images = new int[0];

    public HomeAdapter() {
        this.images = images;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.imageView.setBackgroundResource(images[position]);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.getAbsoluteAdapterPosition() == 0) {
                    NavController navController = Navigation.findNavController(view);
                    navController.navigate(R.id.action_nav_home_to_nav_heroes);
                }
                else {
                    Navigation.findNavController(view).navigate(R.id.action_nav_home_to_nav_items);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.ivHome);
        }
    }

    public void SetItems(int[] items) {
        this.images = items;
        notifyDataSetChanged();
    }

}
