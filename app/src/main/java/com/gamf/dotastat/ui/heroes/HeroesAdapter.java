package com.gamf.dotastat.ui.heroes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gamf.dotastat.R;
import com.gamf.dotastat.models.heroes.Hero;

import java.util.ArrayList;
import java.util.List;

public class HeroesAdapter extends RecyclerView.Adapter<HeroesAdapter.HeroesHolder> {
    private List<Hero> heroes = new ArrayList<>();

    @NonNull
    @Override
    public HeroesAdapter.HeroesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View heroView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_heroes, parent, false);

        return new HeroesHolder(heroView);
    }

    @Override
    public void onBindViewHolder(@NonNull HeroesAdapter.HeroesHolder holder, int position) {
        Hero currentHero = heroes.get(position);

        holder.tvHero.setText(currentHero.getLanguage().getDisplayName());

        Glide.with(holder.itemView)
                .load(currentHero.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivHero);
    }

    @Override
    public int getItemCount() {
        return heroes.size();
    }

    public void SetHeroes(List<Hero> heroes) {
        this.heroes = heroes;
        notifyDataSetChanged();
    }

    public static class HeroesHolder extends RecyclerView.ViewHolder {
        public TextView tvHero;
        public ImageView ivHero;

        public HeroesHolder(@NonNull View heroView) {
            super(heroView);

            tvHero = heroView.findViewById(R.id.tvHero);
            ivHero = heroView.findViewById(R.id.ivHeroes);

            heroView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    int position = getAbsoluteAdapterPosition();
                    bundle.putString("position", String.valueOf(position));
                    Navigation.findNavController(view).navigate(R.id.action_nav_heroes_to_nav_heroes_details, bundle);
                }
            });
        }


    }
}
