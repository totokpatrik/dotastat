package com.gamf.dotastat.ui.gallery;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GalleryViewModel extends ViewModel {

    private final MutableLiveData<String> mText;
    private final MutableLiveData<String> mTextOther;

    public GalleryViewModel() {
        mText = new MutableLiveData<>();
        mTextOther = new MutableLiveData<>();

        mText.setValue("This is gallery fragment");
        mTextOther.setValue("This is another text");
    }

    public LiveData<String> getText() {
        return mText;
    }
    public LiveData<String> getOtherText() {
        return mTextOther;
    }
}