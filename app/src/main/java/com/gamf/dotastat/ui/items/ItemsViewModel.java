package com.gamf.dotastat.ui.items;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gamf.dotastat.repositories.ItemRepository;
import com.gamf.dotastat.models.items.Item;

import java.util.ArrayList;
import java.util.List;

public class ItemsViewModel extends AndroidViewModel {
    private ItemRepository itemRepository;
    private LiveData<List<Item>> mList;

    public ItemsViewModel(@NonNull Application application) {
        super(application);
        itemRepository = new ItemRepository();
        mList = itemRepository.getItems();
    }

    public LiveData<List<Item>> getList() {
        return mList;
    }

}
