package com.gamf.dotastat.ui.heroes;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gamf.dotastat.R;
import com.gamf.dotastat.databinding.FragmentGalleryBinding;
import com.gamf.dotastat.databinding.FragmentHeroesBinding;
import com.gamf.dotastat.models.heroes.Hero;

import java.util.List;

public class HeroesFragment extends Fragment {
    private FragmentHeroesBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        HeroesViewModel heroesViewModel =
                new ViewModelProvider(this).get(HeroesViewModel.class);

        binding = FragmentHeroesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ((AppCompatActivity)getActivity()).getSupportActionBar()
                .setSubtitle("");

        RecyclerView recyclerView = binding.ivHeroes;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        HeroesAdapter heroesAdapter = new HeroesAdapter();
        recyclerView.setAdapter(heroesAdapter);
        heroesViewModel.getList().observe(getViewLifecycleOwner(), new Observer<List<Hero>>() {
            @Override
            public void onChanged(List<Hero> heroes) { heroesAdapter.SetHeroes((heroes));}
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}