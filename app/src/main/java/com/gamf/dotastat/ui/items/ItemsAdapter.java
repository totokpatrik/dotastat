package com.gamf.dotastat.ui.items;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gamf.dotastat.R;
import com.gamf.dotastat.models.items.Item;
import com.gamf.dotastat.repositories.ItemRepository;

import java.util.ArrayList;
import java.util.List;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemsHolder> {
    private List<Item> items = new ArrayList<>();

    @NonNull
    @Override
    public ItemsAdapter.ItemsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_items, parent, false);

        return new ItemsHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsAdapter.ItemsHolder holder, int position) {
        Item currentItem = items.get(position);

        holder.tvItem.setText(currentItem.getLanguage().getDisplayName());
        if (currentItem.getLanguage().getLore().size() > 0) {
            holder.tvLore.setText(currentItem.getLanguage().getLore().get(0));
        }

        Glide.with(holder.itemView)
                .load(currentItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivItem);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void SetItems(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    public static class ItemsHolder extends RecyclerView.ViewHolder {

        public TextView tvItem;
        public TextView tvLore;
        public ImageView ivItem;

        public ItemsHolder(@NonNull View itemView) {
            super(itemView);

            tvItem = itemView.findViewById(R.id.tvItem);
            tvLore = itemView.findViewById(R.id.tvLore);
            ivItem = itemView.findViewById(R.id.ivItem);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    int position = getAbsoluteAdapterPosition();
                    bundle.putString("position", String.valueOf(position));
                    Navigation.findNavController(view).navigate(R.id.action_nav_items_to_nav_items_details, bundle);
                }
            });
        }
    }
}
