package com.gamf.dotastat.ui.heroes;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.gamf.dotastat.databinding.FragmentHeroesDetailBinding;
import com.gamf.dotastat.models.heroes.Hero;

import java.util.List;

public class HeroesDetail extends Fragment {
    private FragmentHeroesDetailBinding binding;
    private int position;

    public HeroesDetail() {
        // Now its empty. Later maybe I'll delete it.
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) { super.onCreate(savedInstanceState); }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        HeroesViewModel heroesViewModel =
                new ViewModelProvider(this).get(HeroesViewModel.class);

        binding = FragmentHeroesDetailBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        position = Integer.parseInt(getArguments().getString("position"));

        String notFound = "Not found";

        TextView tvHeroDisplayName = binding.tvHeroDisplayName;
        TextView tvHeroHype = binding.tvHeroHype;
        TextView tvHeroBio = binding.tvHeroBio;
        TextView tvHeroRoleId1 = binding.tvHeroRoleId1;
        TextView tvHeroRoleId2 = binding.tvHeroRoleId2;
        TextView tvHeroRoleId3 = binding.tvHeroRoleId3;
        TextView tvHeroRoleLevel1 = binding.tvHeroRoleLevel1;
        TextView tvHeroRoleLevel2 = binding.tvHeroRoleLevel2;
        TextView tvHeroRoleLevel3 = binding.tvHeroRoleLevel3;
        TextView tvStatsAttackType = binding.tvStatsAttackType;
        TextView tvStatsStartingArmor = binding.tvStatsStartingArmor;
        TextView tvStatsAttackRange = binding.tvStatsAttackRange;
        TextView tvStatsStrengthBase = binding.tvStatsStrengthBase;
        TextView tvAbilitiesId1 = binding.tvAbilitiesId1;
        TextView tvAbilitiesId2 = binding.tvAbilitiesId2;
        TextView tvAbilitiesId3 = binding.tvAbilitiesId3;
        TextView tvAbilitiesId4 = binding.tvAbilitiesId4;
        TextView tvAbilitiesId5 = binding.tvAbilitiesId5;
        TextView tvAbilitiesId6 = binding.tvAbilitiesId6;

        ImageView ivHeroes = binding.ivHeroes;

        heroesViewModel.getList().observe(getViewLifecycleOwner(), new Observer<List<Hero>>() {
            @Override
            public void onChanged(List<Hero> heroes) {

                //get and set Subtitle Hero name
                if (heroes.get(position).getDisplayName() != null)
                {
                    ((AppCompatActivity) getActivity()).getSupportActionBar()
                            .setSubtitle(heroes.get(position).getDisplayName());
                } else {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(notFound);
                }

                //get and set ImageURL
                if (Glide.with(root).load(heroes.get(position).getImageURL()) != null)
                {
                    Glide.with(root)
                            .load(heroes.get(position).getImageURL())
                            .into(ivHeroes);
                } else {

                }

                //get and set Display Name
                if (heroes.get(position).getDisplayName() != null)
                {
                    tvHeroDisplayName.setText(heroes.get(position).getDisplayName());
                } else {
                    tvHeroDisplayName.setText(notFound);
                }

                //get and set Hype
                if (heroes.get(position).getLanguage().getHype() != null)
                {
                    tvHeroHype.setText(
                            Html.fromHtml(
                                    heroes.get(position).getLanguage().getHype()));
                }
                else {
                    tvHeroHype.setText(notFound);
                }

                //get and set Bio
                if (heroes.get(position).getLanguage().getBio() != null)
                {
                    tvHeroBio.setText(
                            Html.fromHtml(
                                    heroes.get(position).getLanguage().getBio()));
                }
                else {
                    tvHeroHype.setText(notFound);
                }

                //get and set RoleId0 and RoleLevel0
                if (heroes.get(position).getRoles() != null){
                    tvHeroRoleId1.setText(
                            Html.fromHtml(String.valueOf(
                                heroes.get(position).getRoles().get(0).getRoleId()))
                    );
                    tvHeroRoleLevel1.setText(
                            Html.fromHtml(String.valueOf(
                                    heroes.get(position).getRoles().get(0).getLevel()))
                    );
                } else {
                    tvHeroRoleId1.setText(
                            Html.fromHtml(String.valueOf(
                                    notFound
                            )));
                    tvHeroRoleLevel1.setText(
                            Html.fromHtml(String.valueOf(
                                    notFound
                            )));
                }

                //get and set RoleId1 and RoleLevel1
                if (heroes.get(position).getRoles() != null){
                    tvHeroRoleId2.setText(
                            Html.fromHtml(String.valueOf(
                                    heroes.get(position).getRoles().get(1).getRoleId()))
                    );
                    tvHeroRoleLevel2.setText(
                            Html.fromHtml(String.valueOf(
                                    heroes.get(position).getRoles().get(1).getLevel()))
                    );
                } else {
                    tvHeroRoleId2.setText(
                            Html.fromHtml(String.valueOf(
                                    notFound
                            )));
                    tvHeroRoleLevel2.setText(
                            Html.fromHtml(String.valueOf(
                                    notFound
                            )));
                }

                //get and set RoleId2 and RoleLevel2
                if (heroes.get(position).getRoles() != null){
                    tvHeroRoleId3.setText(
                            Html.fromHtml(String.valueOf(
                                    heroes.get(position).getRoles().get(2).getRoleId()))
                    );
                    tvHeroRoleLevel3.setText(
                            Html.fromHtml(String.valueOf(
                                    heroes.get(position).getRoles().get(2).getLevel()))
                    );
                } else {
                    tvHeroRoleId3.setText(
                            Html.fromHtml(String.valueOf(
                                    notFound
                            )));
                    tvHeroRoleLevel3.setText(
                            Html.fromHtml(String.valueOf(
                                    notFound
                            )));
                }


                //get and set Attack Type
                if (heroes.get(position).getStat().getAttackType() != null) {
                    tvStatsAttackType.setText(
                            Html.fromHtml(
                                    heroes.get(position).getStat().getAttackType())
                    );
                } else {
                    tvStatsAttackType.setText(notFound);
                }

                //get and set Starting Armor
                if (heroes.get(position).getStat().getStartingArmor() != null) {
                    tvStatsStartingArmor.setText(
                            Html.fromHtml(
                                    String.valueOf(heroes.get(position).getStat().getStartingArmor()))
                    );
                } else {
                    tvStatsStartingArmor.setText(notFound);
                }

                //get and set Strength Base
                if (heroes.get(position).getStat().getAttackRange() != null) {
                    tvStatsAttackRange.setText(
                            Html.fromHtml(
                                    String.valueOf(heroes.get(position).getStat().getAttackRange()))
                    );
                } else {
                    tvStatsAttackRange.setText(notFound);
                }

                //get and set Strength Base
                if (heroes.get(position).getStat().getStrengthBase() != null) {
                    tvStatsStrengthBase.setText(
                            Html.fromHtml(
                                    String.valueOf(heroes.get(position).getStat().getStrengthBase()))
                    );
                } else {
                    tvStatsStrengthBase.setText(notFound);
                }

                //get and set Ability id
                if (heroes.get(position).getAbilities() != null) {
                    tvAbilitiesId1.setText(
                            Html.fromHtml(
                                    String.valueOf(heroes.get(position).getAbilities().get(0).getAbilityId())
                            )
                    );
                    tvAbilitiesId2.setText(
                            Html.fromHtml(
                                    String.valueOf(heroes.get(position).getAbilities().get(1).getAbilityId())
                            )
                    );
                    tvAbilitiesId3.setText(
                            Html.fromHtml(
                                    String.valueOf(heroes.get(position).getAbilities().get(2).getAbilityId())
                            )
                    );
                    tvAbilitiesId4.setText(
                            Html.fromHtml(
                                    String.valueOf(heroes.get(position).getAbilities().get(3).getAbilityId())
                            )
                    );
                    tvAbilitiesId5.setText(
                            Html.fromHtml(
                                    String.valueOf(heroes.get(position).getAbilities().get(4).getAbilityId())
                            )
                    );
                    tvAbilitiesId6.setText(
                            Html.fromHtml(
                                    String.valueOf(heroes.get(position).getAbilities().get(5).getAbilityId())
                            )
                    );
                } else {
                    tvAbilitiesId1.setText(notFound);
                    tvAbilitiesId2.setText(notFound);
                    tvAbilitiesId3.setText(notFound);
                    tvAbilitiesId4.setText(notFound);
                    tvAbilitiesId5.setText(notFound);
                    tvAbilitiesId6.setText(notFound);
                }



            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}