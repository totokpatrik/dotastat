package com.gamf.dotastat.repositories;

import android.util.Log;
import android.util.Property;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gamf.dotastat.api.ItemService;
import com.gamf.dotastat.models.items.Item;
import com.gamf.dotastat.utils.Credentials;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ItemRepository {
    private ItemService itemService;
    private MutableLiveData<List<Item>> mItems;
    private List<Item> mItemList;

    public ItemRepository() {
        mItems = new MutableLiveData<>();
        mItemList = new ArrayList<>();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Credentials.BASE_URL_STRATZ)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        itemService = retrofit.create(ItemService.class);
        Call<String> stringCall = itemService.ListItems(Credentials.BEARER_TOKEN);

        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String mResponse = response.body();
                //Processing json response half-manually due to shit architecture
                try {
                    JSONObject jsonObject = new JSONObject(mResponse);
                    Gson gson = new GsonBuilder()
                            .serializeNulls()
                            .create();

                    Iterator<String> keys = jsonObject.keys();
                    while (keys.hasNext()) {
                        String key = keys.next();
                        if (jsonObject.get(key) instanceof JSONObject) {
                            String json = jsonObject.getString(key);
                            Item item = gson.fromJson(json, Item.class);
                            mItemList.add(item);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //manually adding the image URL
                for (Item item : mItemList) {
                    if (item.getStat() == null) {
                        item.setImageURL("https://www.dotabuff.com/assets/items/recipe-fd9b7a6bfa829f8757eb5edb907e2c053a9779238a47d4c77b13fd1e0dafc9a2.jpg");
                        continue;
                    }

                    if (item.getStat().isRecipe()) {
                        item.setImageURL("https://www.dotabuff.com/assets/items/recipe-fd9b7a6bfa829f8757eb5edb907e2c053a9779238a47d4c77b13fd1e0dafc9a2.jpg");
                    } else {
                        item.setImageURL("https://cdn.stratz.com/images/dota2/items/"
                                + item.getShortName() + ".png");
                    }
                }

                //end of processing
                mItems.setValue(mItemList);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.getStackTraceString(t);
            }
        });

    }

    public LiveData<List<Item>> getItems() {
        return mItems;
    }
}
