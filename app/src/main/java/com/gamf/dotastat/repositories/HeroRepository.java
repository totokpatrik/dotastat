package com.gamf.dotastat.repositories;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gamf.dotastat.api.HeroService;
import com.gamf.dotastat.api.ItemService;
import com.gamf.dotastat.models.heroes.Hero;
import com.gamf.dotastat.utils.Credentials;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class HeroRepository {
    private HeroService heroService;
    private MutableLiveData<List <Hero> > mHero;
    private List<Hero> mHeroList;

    public HeroRepository() {
        mHero = new MutableLiveData<>();
        mHeroList = new ArrayList<>();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Credentials.BASE_URL_STRATZ)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        heroService = retrofit.create(HeroService.class);
        Call<String> stringCall = heroService.ListHeroes(Credentials.BEARER_TOKEN);

        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String mResponse = response.body();
                //Processing json response half-manually due to shit xd
                try {
                    JSONObject jsonObject = new JSONObject(mResponse);
                    Gson gson = new GsonBuilder()
                            .serializeNulls()
                            .create();
                    Iterator<String> keys = jsonObject.keys();
                    while (keys.hasNext()) {
                        String key = keys.next();
                        if (jsonObject.get(key) instanceof JSONObject) {
                            String json = jsonObject.getString(key);
                            Hero hero = gson.fromJson(json, Hero.class);
                            mHeroList.add(hero);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Manually adding the image URL
                for (Hero hero : mHeroList) {
                    if (hero.getDisplayName() == null)
                    {
                        hero.setImageURL("https://www.dotabuff.com/assets/heroes/pudge-c0a4fce1b8a478ae8da6d61b8d514d070dc58b179abf8eee78f1ff217d14e46c.jpg");
                        continue;
                    }

                    if (hero.getShortName() == null)
                    {
                        hero.setImageURL("https://www.dotabuff.com/assets/heroes/pudge-c0a4fce1b8a478ae8da6d61b8d514d070dc58b179abf8eee78f1ff217d14e46c.jpg");
                    } else {
                        hero.setImageURL("https://cdn.dota2.com/apps/dota2/images/heroes/"
                                + hero.getShortName() + "_sb.png");
                        //https://www.dotafire.com/images/hero/icon/
                        //https://www.dotabuff.com/assets/heroes/
                    }

                }

                //end of processing
                mHero.setValue(mHeroList);

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.getStackTraceString(t);
            }
        });
    }

    public LiveData<List<Hero>> getItems() {return mHero;}
}
