package com.gamf.dotastat.repositories;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gamf.dotastat.api.GamesDetailService;
import com.gamf.dotastat.api.GamesService;
import com.gamf.dotastat.models.games.Match;
import com.gamf.dotastat.models.games.MatchHistory;
import com.gamf.dotastat.models.gamesDetails.MatchDetail;
import com.gamf.dotastat.models.gamesDetails.Player;
import com.gamf.dotastat.utils.Credentials;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class GamesRepository {
    private GamesService gamesService;
    private GamesDetailService gamesDetailService;
    private List<MatchDetail> matchDetails;
    private List<MatchHistory> matchHistory;

    private MutableLiveData<MatchDetail> specificGame;

    private String secondResponse;

    private MutableLiveData<List<Match>> mGames;
    private MutableLiveData<List<MatchDetail>> mGamesDetail;

    public GamesRepository() {
        mGames = new MutableLiveData<>();
        mGamesDetail = new MutableLiveData<>();
        specificGame = new MutableLiveData<>();
        matchHistory = new ArrayList<>();
        matchDetails = new ArrayList<>();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Credentials.BASE_URL_STEAM)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        gamesService = retrofit.create(GamesService.class);
        gamesDetailService = retrofit.create(GamesDetailService.class);

        Call<String> stringCall = gamesService.ListGames(Credentials.STEAM_API_KEY, 10, 20);

        stringCall.enqueue(new Callback<String>() {
            @Override
           public void onResponse(Call<String> call, Response<String> response) {
                String mResponse = response.body();

                Gson gson = new GsonBuilder()
                        .serializeNulls()
                        .create();

                MatchHistory history =  gson.fromJson(mResponse, MatchHistory.class);

                matchHistory.add(history);

                mGames.setValue(history.getResult().getMatches());

                for (Match match: history.getResult().getMatches()) {
                    Call<String> callGamesDetail =
                            gamesDetailService.ListDetails(Credentials.STEAM_API_KEY,
                                    match.getMatchId());

                    callGamesDetail.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            MatchDetail matchDetail = gson.fromJson(response.body(), MatchDetail.class);
                            matchDetails.add(matchDetail);
                            mGamesDetail.setValue(matchDetails);
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.getStackTraceString(t);
            }
        });
    }

    public LiveData<List<MatchDetail>> getGames() {
        return mGamesDetail;
    }

    public LiveData<MatchDetail> getSpecificGame(Long matchId) {
        Call<String> callSpeicificGame =
                gamesDetailService.ListDetails(Credentials.STEAM_API_KEY,
                        matchId);

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .create();


        callSpeicificGame.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                MatchDetail matchDetail = gson.fromJson(response.body(), MatchDetail.class);
                specificGame.setValue(matchDetail);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.getStackTraceString(t);
            }
        });

        return specificGame;
    }
}

